/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CMN_LINK_NODE_HH
#define CMN_LINK_NODE_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <list>

#include "defines.hh"
#include "node.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// shared pointer definition
	typedef std::shared_ptr<class LinkNode> ShLinkNodePr;

	// circle distance function for distmesh
	class LinkNode: public Node{
		// properties
		private:
			arma::uword idx_;

		public:
			// regular constructors
			LinkNode(const arma::uword idx);
			static ShLinkNodePr create(const arma::uword idx);

			// serialization
			Json::Value serialize() const;
			static std::string get_type();
			arma::uword get_index() const;
			LinkNode(const Json::Value &js);
			static ShNodePr create(const Json::Value &js);

			// list operations
			static void add_node(Json::Value &js, std::list<ShNodePr> &list, ShNodePr node);
			static ShNodePr get_node(const Json::Value &js, std::list<ShNodePr> &list); 

	};

}}

#endif
