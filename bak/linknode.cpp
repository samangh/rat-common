/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "linknode.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// constructor
	LinkNode::LinkNode(const arma::uword idx){
		idx_ = idx;
	}
	
	// factory function
	ShLinkNodePr LinkNode::create(const arma::uword idx){
		return std::make_shared<LinkNode>(idx);
	}

	// get index
	arma::uword LinkNode::get_index() const{
		return idx_;
	}

	// type
	std::string LinkNode::get_type(){
		return "linknode";
	}

	// link node
	Json::Value LinkNode::serialize() const{
// create json node
		Json::Value js;	
		
		// store type ID
		js["type"] = get_type();
		js["index"] = (unsigned int)idx_;
		
		// return
		return js;
	}

	// constructor
	LinkNode::LinkNode(const Json::Value &js){
		idx_ = js["index"].asUInt64();
	}

	// factory function
	ShNodePr LinkNode::create(const Json::Value &js){
		return std::make_shared<LinkNode>(js);
	}


	// add a node to the list if it is already on the list it will become a link to that node
	void LinkNode::add_node(Json::Value &js, std::list<ShNodePr> &list, ShNodePr node){
		// find if the node is in the list
		std::list<ShNodePr>::iterator it = 
			std::find(list.begin(), list.end(), node);

		// add supplied node to list
		if(it==list.end()){
			 node->serialize_nodes(list); list.push_back(node);
		}

		// create a link
		else{
			list.push_back(LinkNode::create(std::distance(list.begin(), it)));
		}
	}
	
	// retreive a node from the list, if it is a link it will be taken from its index
	ShNodePr LinkNode::get_node(const Json::Value &js, std::list<ShNodePr> &list){
		// allocate output node pointer
		ShNodePr node;

		// try to downcast node to link
		ShLinkNodePr possible_link = std::dynamic_pointer_cast<LinkNode>(list.back());

		// in case of a regular node
		if(possible_link==NULL){
			node = list.back(); list.pop_back(); node->deserialize_nodes(list);
		}

		// in case of a link
		else{
			// check length
			const arma::uword idx = possible_link->get_index();

			// check length of list
			if(idx>=list.size())rat_throw_line("linking failed element not on list");

			// Create iterator pointing to first element
			std::list<ShNodePr>::iterator it = list.begin(); std::advance(it, idx);
		
			// take node from iterator
			node = (*it);

			// remove link from list
			list.pop_back();
		}


		// return node
		return node;
	}

}}

