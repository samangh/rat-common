![Logo](./figs/RATLogo.png)
# Rat Common Library
<em>Common files, pah, rat thinks is like common cheese ...</em>

## Introduction
Contains common files for the Rat project.

## Installation
As this library is part of a collection of inter-dependent libraries a detailed description of the installation process is provided in a separate repository located here: [rat-docs](https://gitlab.com/Project-Rat/rat-documentation). A quick build and installation is achieved by using

```
mkdir release
cd release
cmake -DCMAKE_BUILD_TYPE=T ..
make -jX
```

where X is the number of cores you wish to use for the build and T is the build type. When the library is build run unit tests and install using

```
make test
sudo make install
```

## ToDo list:
Nothing to do

## Contribution
Not yet.

## Versioning
We're still in alpha.

## Authors
* Jeroen van Nugteren

## License
This project is licensed under the GNU public license.
