/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// this file was evolved from:
// http://math.nist.gov/iml++/

// include header file
#include "gmshfile.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// constructor
	GmshFile::GmshFile(const boost::filesystem::path &fname){
		// open file for writing
		fid_.open(fname.string());

		// write header 
		fid_ << "$MeshFormat\n";
		fid_ << "2.0 0 8\n";
		fid_ << "$EndMeshFormat\n";
	}

	// destructor
	GmshFile::~GmshFile(){
		fid_.close();
	}

	// factory
	ShGmshFilePr GmshFile::create(const boost::filesystem::path &fname){
		return std::make_shared<GmshFile>(fname);
	}

	// write nodes
	void GmshFile::write_nodes(
		const arma::Mat<double> &Rn){

		// get number of nodes
		const arma::uword num_nodes = Rn.n_cols;

		// write header
		fid_ << "$Nodes\n";

		// write node data
		fid_ << num_nodes<<"\n";
		for(arma::uword i=0;i<num_nodes;i++){
			fid_ << i+1 << " " << Rn(0,i) << " " << Rn(1,i) << " " << Rn(2,i) << "\n";
		}

		// footer
		fid_ << "$EndNodes\n";
	}

	// write elements without ID
	void GmshFile::write_elements(
		const arma::Mat<arma::uword> &n){
		// create list of id's all zero
		const arma::Row<arma::uword> id(n.n_cols, arma::fill::zeros);

		// use general method
		write_elements(n,id);
	}

	// write elements
	void GmshFile::write_elements(
		const arma::Mat<arma::uword> &n, 
		const arma::Row<arma::uword> &id){

		// check input 
		assert(n.n_cols==id.n_cols);

		// number of elements
		const arma::uword num_elements = n.n_cols;

		// write header
		fid_ << "$Elements" << "\n";

		// write elements 
		// (elm-number elm-type number-of-tags < tag > ... node-number-list)
		fid_ << num_elements << "\n";
		for(arma::uword i=0;i<num_elements;i++){
			fid_ << i+1 << " ";
			if(n.n_rows==4)fid_ << 3; 
			else if(n.n_rows==8)fid_ << 5; 
			else if(n.n_rows==3)fid_ << 2;
			fid_ << " " << 2 << " " << 99 << " " << id(i) << " ";
			for(arma::uword j=0;j<n.n_rows;j++){
				fid_ << n(j,i)+1 << " ";
			}
			fid_ << "\n";
		}

		// write footer
		fid_ << "$EndElements\n";
	}

	// write elements
	void GmshFile::write_elements(
		const arma::Mat<arma::uword> &n, 
		const arma::Row<arma::uword> &id, 
		const arma::Mat<arma::uword> &s, 
		const arma::Row<arma::uword> &sid){

		// check input 
		assert(n.n_cols==id.n_cols);
		assert(s.n_cols==sid.n_cols);

		// number of elements
		arma::uword num_volume_elements = n.n_cols;
		arma::uword num_surface_elements = s.n_cols;

		// write header
		fid_ << "$Elements" << "\n";

		// number of elements
		fid_ << num_volume_elements + num_surface_elements << "\n";

		// write elements 
		// (elm-number elm-type number-of-tags < tag > ... node-number-list)
		for(arma::uword i=0;i<num_volume_elements;i++){
			fid_ << i+1 << " ";
			if(n.n_rows==4)fid_ << 3; 
			else if(n.n_rows==8)fid_ << 5; 
			else if(n.n_rows==3)fid_ << 2;
			fid_ << " " << 2 << " " << 99 << " " << id(i) << " ";
			for(arma::uword j=0;j<n.n_rows;j++){
				fid_ << n(j,i)+1 << " ";
			}
			fid_ << "\n";
		}

		// write surface elements 
		// (elm-number elm-type number-of-tags < tag > ... node-number-list)
		for(arma::uword i=0;i<num_surface_elements;i++){
			fid_ << num_volume_elements+i+1 << " ";
			if(s.n_rows==4)fid_ << 3; 
			else if(s.n_rows==8)fid_ << 5; 
			else if(s.n_rows==3)fid_ << 2;
			fid_ << " " << 2 << " " << 99 << " " << sid(i) << " ";
			for(arma::uword j=0;j<s.n_rows;j++){
				fid_ << s(j,i)+1 << " ";
			}
			fid_ << "\n";
		}

		// write footer
		fid_ << "$EndElements\n";
	}

	// write scalar-data at nodes
	void GmshFile::write_nodedata(
		const arma::Mat<double> &v,
		const std::string &datname){

		// number of nodes
		const arma::uword num_nodes = v.n_cols;

		// get dimensionality of data
		const arma::uword num_dim = v.n_rows;

		// header
		fid_ << "$NodeData" << std::endl;
		fid_ << 1 << std::endl;
		fid_ << "\"" << datname << "\"" << std::endl;
		fid_ << 1 << std::endl;
		fid_ << 0.0 << std::endl;
		fid_ << 3 << std::endl;
		fid_ << 0 << std::endl;

		// write data
		fid_ << num_dim << "\n";
		fid_ << num_nodes << std::endl;
		for(arma::uword i=0;i<num_nodes;i++){
			fid_ << i+1;
			for(arma::uword j=0;j<num_dim;j++)
				fid_ << " " << v(j,i);
			fid_ << "\n";
		}

		// footer
		fid_ << "$EndNodeData" << std::endl;
	}

	// write vector-data at elements
	void GmshFile::write_elementdata(
		const arma::Mat<double> &v,
		const std::string &datname){

		// number of nodes
		const arma::uword num_elements = v.n_cols;

		// get dimensionality of data
		const arma::uword num_dim = v.n_rows;

		// header
		fid_ << "$ElementData\n";
		fid_ << 1 << "\n";
		fid_ << "\"" << datname << "\"" << std::endl;
		fid_ << 1 << "\n";
		fid_ << 0.0 << "\n";
		fid_ << 3 << "\n";
		fid_ << 0 << "\n";

		// write data
		fid_ << num_dim << "\n";
		fid_ << num_elements << "\n";
		for(arma::uword i=0;i<num_elements;i++){
			fid_ << i+1;
			for(arma::uword j=0;j<num_dim;j++)
				fid_ << " " << v(j,i);
			fid_ << "\n";
		}

		// footer
		fid_ << "$EndElementData\n";
	}

}}