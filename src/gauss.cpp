/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "gauss.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// default constructor
	Gauss::Gauss(){
		// setup points
		calculate();
	}

	// constructor
	Gauss::Gauss(arma::sword num_gauss){
		// check input
		assert(num_gauss!=0);

		// set number of gauss points
		num_gauss_ = num_gauss;

		// setup points
		calculate();
	}


	// Generates the abscissa and weights for a Gauss-Legendre quadrature.
	// Reference:  Numerical Recipes in Fortran 77, Cornell press.
	void Gauss::calculate(){
		// can not be zero
		assert(num_gauss_!=0);

		// get number of gauss points
		arma::uword ng = (arma::uword)std::abs(num_gauss_);

		// gauss point distribution
		if(num_gauss_>0){
			// allocate output
			xg_.zeros(ng); wg_.zeros(ng);

			// calculate
			double m = (ng+1.0)/2;
			for(arma::uword ii=0;ii<m;ii++){
				// initial estimate
				double z = std::cos(arma::datum::pi*(ii+1-0.25)/(ng+0.5));                      
				double z1 = z+1;
				double pp = 0; // not used
				while(std::abs(z-z1)>tol_){
					double p1 = 1, p2 = 0;
					for(arma::uword jj=1;jj<=ng;jj++){
						double p3 = p2;
						p2 = p1;

						// The Legendre polynomial.
						p1 = ((2*jj-1)*z*p2-(jj-1)*p3)/jj;       
					}

					// The L.P. derivative.
					pp = ng*(z*p1-p2)/(z*z-1);                       
					z1 = z;
					z = z1-p1/pp;
				}

				// Build up the abscissas.
				xg_(ii) = -z;
				xg_(ng-1-ii) = z;

				// Build up the weights.
				wg_(ii) = 2/((1-z*z)*(pp*pp));
				wg_(ng-1-ii) = wg_(ii);
			}
		}

		// regular distribution
		if(num_gauss_<0){
			xg_ = arma::linspace<arma::Row<double> >(-1.0 + 1.0/ng, 1.0 - 1.0/ng,ng);
			wg_ = arma::Row<double>(ng,arma::fill::ones)*2.0/ng;
		}
	}

	// function for getting absissas
	arma::Row<double> Gauss::get_abscissae() const{
		return xg_;
	}

	// function for getting absissas
	arma::Row<double> Gauss::get_weights() const{
		return wg_;
	}

}}