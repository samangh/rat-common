/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "node.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// serialization
	void Node::serialize(Json::Value &js, SList &) const{
		js["type"] = "cmn::node";
	}
	
	// desieralization
	void Node::deserialize(
		const Json::Value &/*js*/, 
		std::map<arma::uword,ShNodePr> &/*list*/, 
		const NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/){
		// nothing happens
	}

	// default copy function
	ShNodePr Node::copy() const{
		rat_throw_line("default copy function not overidden");
		return std::make_shared<Node>();
	}

	// serialization of armadillo row vectors
	Json::Value Node::serialize_arma(const arma::Row<double> &V){
		Json::Value js; 
		for(arma::uword i=0;i<V.n_elem;i++)js.append(V(i));
		return js;
	}

	// deserialization of armadillo row vectors
	arma::Row<double> Node::deserialize_arma(const Json::Value &js){
		const arma::uword n = js.size();
		arma::Row<double> V(n);
		for(arma::uword i=0;i<n;i++)V(i) = js.get(i,0).asDouble();
		return V;
	}

	// serialize nodes
	Json::Value Node::serialize_node(ShNodePr node, SList &list){
		// allocate output json
		Json::Value js; 

		// in case of Null
		if(node==NULL){
			js["type"] = "cmn::null"; return js;
		}

		// if it is not on the list add the node to the list and serialize it
		if(list.count(node)==0){
			node->serialize(js,list); 
			arma::uword id = list.size();
			list[node] = id;
			js["id"] = (int)id;
		}

		// create a link
		else{
			js["type"] = "cmn::link";
			js["target_index"] = (int)list.at(node);
		}

		// return json
		return js;
	}

	// deserialize nodes
	ShNodePr Node::deserialize_node_core(
		const Json::Value &js, DSList &list, 
		const NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){

		// check if it is an array of objects
		if(js.isArray()){
			rat_throw_line("input json node is array");
		}

		// check if json input is NULL
		if(js.isNull()){
			return NULL;
		}

		// check if the json input is a file instead of an object
		Json::Value js_object = js;
		boost::filesystem::path next_path = pth;
		if(js.isString()){
			boost::filesystem::path filepath(js.asString());
			if(filepath.empty())rat_throw_line("object is a serialized as string but is not a filename");
			if(filepath.is_relative())filepath = boost::filesystem::absolute(pth/filepath);
			next_path = filepath.parent_path();
			js_object = parse_json(filepath);
		}

		//.check 
		if(!js_object.isObject())rat_throw_line("json is not an object");

		// get type string
		const std::string type = js_object["type"].asString();

		// allocate output node pointer
		ShNodePr node;

		// check if string exists
		if(type.length()==0){
			rat_throw_line("type is empty string");
		}
		
		// check for node this means that the type was not specified in class
		else if(type.compare("cmn::node")==0){
			rat_throw_line("type not specified");
		}
			
		// check for null node
		else if(type.compare("cmn::null")==0){
			node = NULL;
		}

		// in case of a link
		else if(type.compare("cmn::link")==0){
			// check length
			const arma::uword idx = js_object["target_index"].asUInt64();

			// Create iterator pointing to first element
			node = list.at(idx);
		}

		// in case of a regular node
		else{
			// find the factory based on the type string
			const std::string type = js_object["type"].asString();
			NodeFactoryMap::const_iterator it = factory_list.find(type);
			
			// check if the factory was found and return
			if(it!=factory_list.end())node = it->second();
			else rat_throw_line("type not registered: " + type);

			// now deserialize it
			node->deserialize(js_object,list,factory_list,next_path);

			// add node to list
			list[js_object["id"].asUInt64()] = node;
		}

		// return node
		return node;
	}

	// static json parse function
	Json::Value Node::parse_json(const boost::filesystem::path &fname){
		// create reader
		Json::CharReaderBuilder rbuilder;
		
		// open file for reading
		std::ifstream if_id(fname.string(), std::ifstream::in);
		if(!if_id.good())rat_throw_line("input file doesn't exist: " + fname.string());

		// allocate json
		Json::Value js;

		// write
		std::string errs;
		bool ok = Json::parseFromStream(rbuilder, if_id, &js, &errs);

		// close input file
		if_id.close();

		// check for errors
		if(ok==false)rat_throw_line("json parsing error: " + fname.string() + errs);

		// return json
		return js;
	}

}}
