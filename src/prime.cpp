/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "prime.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// prime number generator
	// based on Sieve Of Eratosthenes algorithm
	arma::Row<arma::uword> Prime::eratosthenes(arma::uword N){

		// create the sieve
	    arma::Row<unsigned char> sieve(N+1,arma::fill::ones);
	 	
	    // zero and one are not primes
	    sieve(0) = 0; sieve(1) = 0;

	 	// calculate 	
	    for (arma::uword p=2; p*p<=N; p++){
	        // If prime[p] is not changed, then it is a prime
	        if (sieve(p) == 1){
	            // Update all multiples of p
	            for (arma::uword i=p*2; i<=N; i += p)
	                sieve(i) = 0;
	        }
	    }

	    // get primes
	 	arma::Row<arma::uword> primes = arma::find(sieve==1).t();

	 	// return
	    return primes;
	}

	// get first N primes
	arma::Row<arma::uword> Prime::calculate(arma::uword N){
		// estimate n-prime max using prime number theorem
		// to invert this theorem one can use "lambertw" functions
		// we use simple interpolation instead
		arma::Col<double> nlist = arma::logspace<arma::Col<double> >(0,12);
		arma::Col<double> pi = nlist/arma::log(nlist);
		arma::Col<double> x; arma::Col<double> Na = {(double)N};
		arma::interp1(pi,nlist,Na,x,"linear",0);
		arma::uword num_sieve = arma::as_scalar(x);

		// run algorithm to calculate list of prime numbers
		arma::Row<arma::uword> primes = Prime::eratosthenes((arma::uword)std::ceil(1.15*num_sieve));
		//std::cout<<arma::as_scalar(primes.tail(1))/(1.15*num_sieve)<<std::endl;

		// cut list short
		assert(primes.n_elem>=N);
		primes = primes.cols(0,N-1);

		// return list
		return primes;
	}

}}