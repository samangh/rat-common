/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "opera.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// constructor
	Opera::Opera(const std::string &fname){
		// open file for writing
		fid_.open(fname);
		
		// ensure file is open
		assert(fid_.is_open());

		// set precision
		fid_ << std::fixed;
		fid_ << std::setprecision(6);

		// conductor file
		fid_ << "CONDUCTOR\n";

		// set first drive
		drive_idx_ = 0;
	}

	// destructor
	Opera::~Opera(){
		// write footer
		fid_ << "QUIT\n";

		// close file
		if(fid_.is_open())fid_.close();
	}

	// factory
	ShOperaPr Opera::create(const std::string &fname){
		//return ShCalcFreeCADPr(new CalcFreeCAD);
		return std::make_shared<Opera>(fname);
	}

	// write to output file
	void Opera::write_edges(
		const arma::field<arma::Mat<double> > x,
		const arma::field<arma::Mat<double> > y,
		const arma::field<arma::Mat<double> > z,
		const double J){

		// walk over coil sections
		for(arma::uword j=0;j<x.n_elem;j++){
			// walk over edges
			for(arma::uword k=0;k<x(j).n_cols-1;k++){
				fid_ << "DEFINE BR8\n";

				// settings
				fid_ << "0.0 0.0 0.0 0.0 0.0 0.0\n";
				fid_ << "0.0 0.0 0.0\n";
				fid_ << "0.0 0.0 0.0\n";

				// nodes in first face
				for(arma::uword l=0;l<4;l++)
					fid_ << x(j)(l,k) << " " << y(j)(l,k) << " " << z(j)(l,k) << "\n";

				// nodes in second face
				for(arma::uword l=0;l<4;l++)
					fid_ << x(j)(l,k+1) << " " << y(j)(l,k+1) << " " << z(j)(l,k+1) << "\n";

				// current 
				fid_ << J << " 1 'drive " << drive_idx_ << "'\n";

				// mirror settings
				fid_ << "0 0 0\n"; // no mirror
				fid_ << "1e-6\n"; // tolerance
			}
		}

		// increment drive
		drive_idx_++;
	}

}}