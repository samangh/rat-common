/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "serializer.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// constructor
	Serializer::Serializer(){
		register_constructors();
	}

	// constructor
	Serializer::Serializer(ShNodePr root_node){
		register_constructors(); flatten_tree(root_node);
	}

	// constructor
	Serializer::Serializer(const boost::filesystem::path &fname){
		register_constructors(); import_json(fname);
	}

	// factory
	ShSerializerPr Serializer::create(){
		return std::make_shared<Serializer>();
	}

	// factory
	ShSerializerPr Serializer::create(ShNodePr root_node){
		return std::make_shared<Serializer>(root_node);
	}

	// factory
	ShSerializerPr Serializer::create(const boost::filesystem::path &fname){
		return std::make_shared<Serializer>(fname);
	}

	// serialize
	void Serializer::flatten_tree(ShNodePr root_node){
		// reset root node
		root_json_.clear();

		// create all nodes
		SList node_list;
		root_json_["tree"] = Node::serialize_node(root_node, node_list);

		// add version number
		root_json_["version"] = json_version_;
		root_json_["num_nodes"] = (int)node_list.size();
	}

	// deserialize
	ShNodePr Serializer::construct_tree_core(){
		// check registry
		if(factory_list_.size()==0)rat_throw_line("no classes registered for construction");

		// allocate list of nodes
		DSList node_list;

		// create directory
		boost::filesystem::path pth = pth_;

		// create root node
		ShNodePr root_node = Node::deserialize_node<Node>(root_json_["tree"],node_list,factory_list_,pth);

		// check if number of nodes set
		if(!root_json_["num_nodes"].isNull()){
			// check remaining length
			const arma::uword num_nodes = root_json_["num_nodes"].asUInt64();
			if(node_list.size()!=num_nodes)
				rat_throw_line("number of nodes incorrect: " + std::to_string(node_list.size()));
		}

		// return the root node
		return root_node;
	}

	// loading input files
	void Serializer::import_json(const boost::filesystem::path &fname){
		// load json file
		root_json_ = Node::parse_json(fname);
		if(root_json_["tree"].isNull()){
			Json::Value new_root;
			new_root["tree"] = root_json_;
			root_json_ = new_root;
		}

		// set path of the root file
		pth_ = boost::filesystem::path(fname).parent_path();
	}

	// writing input files
	void Serializer::export_json(const boost::filesystem::path &fname){
		// create a streamwriter
		Json::StreamWriterBuilder builder;
		const std::unique_ptr<Json::StreamWriter> 
			writer(builder.newStreamWriter());

		// open file
		std::ofstream of_id(fname.string());

		// write to file
		writer->write(root_json_, &of_id);

		// close file stream
		of_id.close();
	}
	
	// registration
	void Serializer::register_factory(
		const std::string &str,NodeFactory factory){
		// check if already on the list
		// NodeFactoryMap::iterator it = factory_list_.find(str);
		// if(it!=factory_list_.end())rat_throw_line("duplicate nodefactory being registered: " + str);

		// check if already on the list
		if(factory_list_.count(str)>0)rat_throw_line("duplicate nodefactory being registered: " + str);

		// add to list
		factory_list_[str] = factory;
	}

	// print registered types
	void Serializer::list_factories(ShLogPr lg) const{
		lg->msg(2,"%sserializer factory list%s\n",KBLU,KNRM);
		lg->msg("number of entries: %llu\n",factory_list_.size());
		for(auto it = factory_list_.begin();it!=factory_list_.end();it++)
			lg->msg("%s\n",(*it).first.c_str());
		lg->msg(-2,"\n");
	}

	// get factory list
	NodeFactoryMap Serializer::get_factory_list() const{
		return factory_list_;
	}

}}