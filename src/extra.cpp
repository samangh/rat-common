/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "extra.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// rounding function for C++97
	double Extra::round(const double d){
		return std::floor(d + 0.5);
	}

	// sign function
	double Extra::sign(const double s){
		double ss;
		if(s>0)ss=1.0;
		else if(s<0)ss=-1.0;
		else ss=0.0;
		return ss;
	}
	
	// custom cross product that allows 3XN vectors as input (similar to Matlab)
	// this is not possible in the armadillo library
	// https://en.wikipedia.org/wiki/Cross_product
	arma::Mat<double> Extra::cross(
		const arma::Mat<double> &R1, 
		const arma::Mat<double> &R2){
		assert(R1.n_rows==3); assert(R2.n_rows==3); assert(R1.n_cols==R2.n_cols);

		// allocate output
		arma::Mat<double> M(R1.n_rows, R1.n_cols);

		// calculate cross product
		M.row(0) = R1.row(1)%R2.row(2) - R1.row(2)%R2.row(1);
		M.row(1) = R1.row(2)%R2.row(0) - R1.row(0)%R2.row(2);
		M.row(2) = R1.row(0)%R2.row(1) - R1.row(1)%R2.row(0);

		// return output matrix
		return M;
	}

	// custom dot product that allows 3XN vectors as input (similar to Matlab)
	// this is not possible in the armadillo library
	arma::Row<double> Extra::dot(
		const arma::Mat<double> &R1, 
		const arma::Mat<double> &R2){
		assert(R1.n_rows==2 || R1.n_rows==3);
		assert(R2.n_rows==R1.n_rows); assert(R1.n_cols==R2.n_cols);
		return arma::sum(R1%R2,0);
	}

	// Norm of a vector or 3XN array of vectors
	arma::Row<double> Extra::vec_norm(
		const arma::Mat<double> &R){
		assert(R.n_rows==3 || R.n_rows==2);
		return arma::sqrt(Extra::dot(R,R));
	}

	// display complex armadillo matrix
	void Extra::display_mat(const arma::Mat<std::complex<double> > &M){
		// walk over matrix
		std::printf("Real part\n");
		Extra::display_mat(arma::real(M));

		// walk over matrix
		std::printf("Complex part\n");
		Extra::display_mat(arma::imag(M));

		// whiteline
		std::printf("\n");
	}

	// display real armadillo matrix
	void Extra::display_mat(const arma::Mat<double> &M){
		// walk over matrix
		for (int i=0;i<(int)M.n_rows;i++){
			for (int j=0;j<(int)M.n_cols;j++){
				// check for zero
				double val = M(i,j);
				std::printf("%+01.0e ",val);
			}
			std::printf("\n");
		}

		// whiteline
		std::printf("\n");
	}

	// display complex sparse armadillo matrix
	void Extra::display_mat(const arma::SpMat<std::complex<double> > &M){
		// walk over matrix
		std::printf("Real part\n");
		Extra::display_mat(arma::real(M));

		// walk over matrix
		std::printf("Complex part\n");
		Extra::display_mat(arma::imag(M));

		// whiteline
		std::printf("\n");
	}

	// display real sparse armadillo matrix
	void Extra::display_mat(const arma::SpMat<double> &M){
		// walk over matrix
		for (int i=0;i<(int)M.n_rows;i++){
			for (int j=0;j<(int)M.n_cols;j++){
				// check for zero
				double val = M(i,j);
				if (val==0.0){
					std::printf("...... ");
				}else{
					std::printf("%+01.0e ",val);
				}
			}
			std::printf("\n");
		}

		// whiteline
		std::printf("\n");
	}

	// custom function for finding sections in array
	// for example find_sections([1,1,1,2,2,2,3,3],"first") should give [0,3,6]
	// for example find_sections([1,1,1,2,2,2,3,3],"last") should give [2,5,7]
	arma::Mat<arma::uword> Extra::find_sections(const arma::Row<arma::uword> &M){
		// make sure it is a row vector
		assert(M.n_rows==1);

		// allocate output
		arma::Mat<arma::uword> indices;

		// check if matrix is empty
		if(!M.is_empty()){
			// find indices at changing parts
			arma::Row<arma::uword> idx = find(M.tail_cols(M.n_cols-1)!=M.head_cols(M.n_cols-1)).t(); 
		
			// make first and last indexes
			arma::Row<arma::uword> idx1 = arma::join_horiz(arma::Mat<arma::uword>(1,1,arma::fill::zeros),idx+1);
			arma::Row<arma::uword> idx2 = arma::join_horiz(idx,arma::Mat<arma::uword>(1,1,arma::fill::ones)*(M.n_cols-1));
		
			// return
			indices = arma::join_vert(idx1,idx2);
		}

		// if matrix is empty return empty indices
		else{
			indices = arma::Mat<arma::uword>(2,0);
		}

		// return output matrix with indexes
		return indices;
	}

	// set sections
	// takes output from find_sections and creates origin array
	arma::Mat<arma::uword> Extra::set_sections(const arma::Mat<arma::uword> &indices){
		// check input
		assert(indices.n_rows==2);

		// get last section
		arma::uword num_elem = indices(1,indices.n_cols-1)+1;
		
		// allocate output
		arma::Mat<arma::uword> origin(1,num_elem);

		// fill output
		for(arma::uword i=0;i<indices.n_cols;i++){
			origin.cols(indices(0,i),indices(1,i)).fill(i);
		}
		
		// return output
		return origin;
	}

	// custom bitshift operation as it is not available in armadillo
	arma::Mat<arma::uword> Extra::bitshift(const arma::Mat<arma::uword> &M, const int nshift){
		// // allocate
		// arma::Mat<arma::uword> bs;

		// // use different function for each option
		// if (nshift>=0){
		// 	bs = arma::floor(M*std::pow(2,nshift));
		// }else{
		// 	bs = arma::floor(M/(1.0/std::pow(2,nshift)));
		// }

		// bitshift without pow
		// arma::Mat<arma::uword> bs = M;
		// if(nshift>=0)for(int i=0;i<nshift;i++)bs*=2;
		// else if(nshift<0)for(int i=0;i<-nshift;i++)bs/=2;

		// bitshift with operator
		// allocate
		arma::Mat<arma::uword> bs(M.n_rows, M.n_cols);

		// positive shift
		if(nshift>0)for(arma::uword i=0;i<M.n_elem;i++)bs.at(i) = M.at(i)<<nshift;

		// negative shift
		else if(nshift<0)for(arma::uword i=0;i<M.n_elem;i++)bs.at(i) = M.at(i)>>(-nshift);

		// no shift
		else bs = M;

		// output result
		return bs;
	}

	// function for generating coordinates arround a center point given by xyz coords
	arma::Mat<double> Extra::random_coordinates(
		const double xc, 
		const double yc, 
		const double zc, 
		const double size,
		const arma::uword N){

		// create center point
		arma::Col<double>::fixed<3> Rc = {xc,yc,zc};

		// call overloaded function
		return Extra::random_coordinates(Rc,size,N);
	}

	// function for generating coordinates arround a center point given by vector
	arma::Mat<double> Extra::random_coordinates(
		arma::Col<double>::fixed<3> Rc, 
		const double size,
		const arma::uword N){

		// create coordinates
		arma::Mat<double> R = size*(arma::Mat<double>(3,N,arma::fill::randu)-0.5);
		R = R.each_col() + Rc;

		// return output
		return R; 
	}

	// calculate modulus
	arma::Mat<arma::uword> Extra::modulus(
		const arma::Mat<arma::uword> &A, 
		const arma::uword i){
		
		return A - arma::floor(A/i)*i;
	}

	// create multi-dimensional index lists
	arma::Row<arma::uword> Extra::expand_indices(
		const arma::Mat<arma::uword> &indices, 
		const arma::uword num_dim){ 

		// check index size
		assert(indices.n_rows==1);

		// allocate output
		arma::Mat<arma::uword> dim_indices(num_dim,indices.n_cols);

		// fill each row
		for (arma::uword i=0;i<num_dim;i++){
			dim_indices.row(i) = num_dim*indices + i;
		}

		// reshape output to single row
		dim_indices.reshape(1,num_dim*indices.n_cols);

		// return output
		return dim_indices;
	}



	// matlab cell2mat implementation for armadillo
	template<typename T> 
	T Extra::field2mat(
		const arma::field<T> &fld){
		
		// check if it is necessary to make a new matrix
		if (fld.n_cols==1 && fld.n_rows==1){
			return fld(0,0);
		}

		// get sizes of all submatrices
		arma::Mat<arma::uword> num_rows(fld.n_rows,fld.n_cols);
		arma::Mat<arma::uword> num_cols(fld.n_rows,fld.n_cols);
		for(arma::uword i=0;i<fld.n_rows;i++){
			for(arma::uword j=0;j<fld.n_cols;j++){
				num_rows(i,j) = fld(i,j).n_rows;
				num_cols(i,j) = fld(i,j).n_cols;
			}
		}
		
		// check if submatrices fit
		arma::Col<arma::uword> num_rows_first_col = num_rows.col(0);
		arma::Row<arma::uword> num_cols_first_row = num_cols.row(0);

		// check if matrix fits
		assert(arma::all(arma::all(num_rows_first_col-num_rows.each_col()==0)));
		assert(arma::all(arma::all(num_cols_first_row-num_cols.each_row()==0)));

		// allocate output matrix
		T mt(arma::sum(num_rows_first_col),arma::sum(num_cols_first_row));

		// fill output matrix
		// walk over rows
		arma::uword row_start = 0;
		for(arma::uword i=0;i<fld.n_rows;i++){
			// walk over columns
			arma::uword col_start = 0;
			for(arma::uword j=0;j<fld.n_cols;j++){
				// check if this cell is not empty
				if(num_rows(i,j)>0 && num_cols(i,j)>0){
					// calculate indexes
					arma::uword row_end = row_start + num_rows_first_col(i) - 1;
					arma::uword col_end = col_start + num_cols_first_row(j) - 1;
				
					// insert into submatrix
					mt.submat( row_start, col_start, row_end, col_end) = fld(i,j);

					// increment column index
					col_start += num_cols_first_row(j);
				}		
			}

			// increment row index
			row_start += num_rows_first_col(i);
		}

		// return output matrix
		return mt;
	}

	// explicitly tell compiler which functions to build
	template arma::Mat<arma::uword> Extra::field2mat<arma::Mat<arma::uword> >(const arma::field<arma::Mat<arma::uword> > &fld);
	template arma::Mat<double> Extra::field2mat<arma::Mat<double> >(const arma::field<arma::Mat<double> > &fld);

	// // matlab cell2mat implementation for armadillo
	// arma::Mat<double> Extra::field2mat(
	// 	const arma::field<arma::Mat<double> > &fld){
		
	// 	// check if it is necessary to make a new matrix
	// 	if (fld.n_cols==1 && fld.n_rows==1){
	// 		return fld(0,0);
	// 	}

	// 	// get sizes of all submatrices
	// 	arma::Mat<arma::uword> num_rows(fld.n_rows,fld.n_cols);
	// 	arma::Mat<arma::uword> num_cols(fld.n_rows,fld.n_cols);
	// 	for(arma::uword i=0;i<fld.n_rows;i++){
	// 		for(arma::uword j=0;j<fld.n_cols;j++){
	// 			num_rows(i,j) = fld(i,j).n_rows;
	// 			num_cols(i,j) = fld(i,j).n_cols;
	// 		}
	// 	}
		
	// 	// check if submatrices fit
	// 	arma::Col<arma::uword> num_rows_first_col = num_rows.col(0);
	// 	arma::Row<arma::uword> num_cols_first_row = num_cols.row(0);

	// 	// check if matrix fits
	// 	assert(arma::all(arma::all(num_rows_first_col-num_rows.each_col()==0)));
	// 	assert(arma::all(arma::all(num_cols_first_row-num_cols.each_row()==0)));

	// 	// allocate output matrix
	// 	arma::Mat<double> mt(arma::sum(num_rows_first_col),arma::sum(num_cols_first_row));

	// 	// fill output matrix
	// 	// walk over rows
	// 	arma::uword row_start = 0;
	// 	for(arma::uword i=0;i<fld.n_rows;i++){
	// 		// walk over columns
	// 		arma::uword col_start = 0;
	// 		for(arma::uword j=0;j<fld.n_cols;j++){
	// 			// check if this cell is not empty
	// 			if(num_rows(i,j)>0 && num_cols(i,j)>0){
	// 				// calculate indexes
	// 				arma::uword row_end = row_start + num_rows_first_col(i) - 1;
	// 				arma::uword col_end = col_start + num_cols_first_row(j) - 1;
				
	// 				// insert into submatrix
	// 				mt.submat( row_start, col_start, row_end, col_end) = fld(i,j);

	// 				// increment column index
	// 				col_start += num_cols_first_row(j);
	// 			}		
	// 		}

	// 		// increment row index
	// 		row_start += num_rows_first_col(i);
	// 	}

	// 	// return output matrix
	// 	return mt;
	// }

	// reverse field array helper function (static)
	void Extra::reverse_field(arma::field<arma::Mat<double> > &A){
		// swap fields
		for(arma::uword i=0;i<(arma::uword)std::floor(A.n_elem/2);i++){
			arma::Mat<double> tmp1 = A(i); arma::Mat<double> tmp2 = A(A.n_elem-1-i);
			A(i) = tmp2; A(A.n_elem-1-i) = tmp1;
		}

		// swap columns
		for(arma::uword i=0;i<A.n_elem;i++){
			A(i) = arma::fliplr(A(i));
		}
	}

	// build rotation matrix
	arma::Mat<double>::fixed<3,3> Extra::create_rotation_matrix(
		const double phi, const double theta, const double psi){

		// setup rotation matrix
		arma::Mat<double>::fixed<3,3> Ax,Ay,Az,A;

		// rotation arround x-axis
		Ax.row(0) = arma::Row<double>{1.0,0.0,0.0}; 
		Ax.row(1) = arma::Row<double>{0,std::cos(phi),-std::sin(phi)}; 
		Ax.row(2) = arma::Row<double>{0,std::sin(phi),std::cos(phi)}; 
		
		// rotation arround y-axis
		Ay.row(0) = arma::Row<double>{std::cos(theta),0.0,std::sin(theta)}; 
		Ay.row(1) = arma::Row<double>{0.0,1.0,0.0}; 
		Ay.row(2) = arma::Row<double>{-std::sin(theta),0.0,std::cos(theta)};
		
		// rotation arround z-axis
		Az.row(0) = arma::Row<double>{std::cos(psi),-std::sin(psi),0.0}; 
		Az.row(1) = arma::Row<double>{std::sin(psi),std::cos(psi),0.0}; 
		Az.row(2) = arma::Row<double>{0.0,0.0,1.0};
		
		// combined rotation matrix
		A = Ax*Ay*Az;

		// return matrix
		return A;
	}

	// build rotation matrix for rotation arround axis with vector input
	arma::Mat<double>::fixed<3,3> Extra::create_rotation_matrix(
		const arma::Col<double>::fixed<3> &V, const double alpha){
		return Extra::create_rotation_matrix(V(0),V(1),V(2),alpha);
	}


	// build rotation matrix for rotation arround axis
	// https://en.wikipedia.org/wiki/Rotation_matrix#Conversion_from_and_to_axis%E2%80%93angle
	arma::Mat<double>::fixed<3,3> Extra::create_rotation_matrix(
		const double ux, const double uy, const double uz, const double alpha){
		// check unit vector
		assert(std::abs(std::sqrt(ux*ux+uy*uy+uz*uz)-1.0)<1e-6);

		// setup rotation matrix
		arma::Mat<double>::fixed<3,3> A;

		// setup matrix
		// https://en.wikipedia.org/wiki/Rotation_matrix#Conversion_from_and_to_axis%E2%80%93angle
		A.row(0) = arma::Row<double>::fixed<3>{
			std::cos(alpha)+ux*ux*(1-std::cos(alpha)),
			ux*uy*(1-std::cos(alpha))-uz*std::sin(alpha),
			ux*uz*(1-std::cos(alpha))+uy*std::sin(alpha)};
		A.row(1) = arma::Row<double>::fixed<3>{
			uy*ux*(1-std::cos(alpha))+uz*std::sin(alpha),
			std::cos(alpha)+uy*uy*(1-std::cos(alpha)),
			uy*uz*(1-std::cos(alpha))-ux*std::sin(alpha)};
		A.row(2) = arma::Row<double>::fixed<3>{
			uz*ux*(1-std::cos(alpha))-uy*std::sin(alpha),
			uz*uy*(1-std::cos(alpha))+ux*std::sin(alpha),
			std::cos(alpha)+uz*uz*(1-std::cos(alpha))};

		// return matrix
		return A;
	}


	// interpolation for one scalar
	double Extra::interp1(
		const arma::Mat<double> &X, 
		const arma::Mat<double> &Y, 
		const double XI,
		const char *type,
		const bool extrap){

		// check input
		assert(X.is_sorted());
		assert(XI<X(X.n_elem-1));
		assert(XI>X(0));

		// convert to array
		arma::Col<double> xi(1),yi(1); xi(0) = XI;

		// perform linear interpolation
		Extra::interp1(
			arma::reshape(X,X.n_elem,1),
			arma::reshape(Y,Y.n_elem,1),
			xi,yi,type,extrap);

		// return value
		return yi(0);
	}


	// // get sort index (as defined in armadillo) for matrix for each row
	// // this can be implemented by using multiple sort_index_safe
	// arma::Row<arma::uword> Extra::matrix_sort_index_by_row(
	// 	arma::Mat<arma::uword> M){

	// 	// increment
	// 	M += 1;

	// 	// walk over rows of M
	// 	arma::uword mult_val = 1;
	// 	for(arma::uword i=0;i<M.n_rows;i++){
	// 		arma::uword myrow = M.n_rows-i-1;
	// 		arma::uword maxval = arma::as_scalar(
	// 			arma::max(M.row(myrow),1));
	// 		M.row(myrow) *= mult_val;
	// 		mult_val *= (maxval+1);
	// 	}

	// 	// sort normally
	// 	return arma::sort_index(arma::sum(M,0)).t();
	// }

	// interpolation and or extrapolation
	// only linear, x does not need to be linearly increasing
	void Extra::interp1(
		const arma::Col<double> &x,
		const arma::Col<double> &y,
		const double &xi, double &yi,
		const char *type,
		const bool extrap){
		
		// convert to arrays
		arma::Col<double> yii;
		Extra::interp1(x,y,arma::Col<double>{xi},yii,type,extrap);
		yi = arma::as_scalar(yii);
	}


	// interpolation and or extrapolation
	// only linear, x does not need to be linearly increasing
	void Extra::interp1(
		const arma::Col<double> &x,
		const arma::Col<double> &y,
		const arma::Col<double> &xi,
		arma::Col<double> &yi,
		const char *type,
		const bool extrap){

		// end index
		arma::uword end = x.n_elem-1;

		// normal interpolation
		arma::interp1(x,y,xi,yi,type);

		// if extrapolation
		if(extrap==true){
			// extrapolation beyond end
			const arma::Col<arma::uword> idx1 = arma::find(xi>x(x.n_elem-1));
			yi(idx1) = y(end) + (xi(idx1)-x(end))*((y(end)-y(end-1))/(x(end)-x(end-1)));

			// extrapolation before start
			const arma::Col<arma::uword> idx2 = arma::find(xi<x(0));
			yi(idx2) = y(0) - (x(0)-xi(idx2))*((y(1)-y(0))/(x(1)-x(0)));
		}
	}

	// interpolation assuming X is monotonically increasing
	// but not XI like armadillo 
	void Extra::lininterp1f(
		const arma::Col<double> &x,
		const arma::Col<double> &y,
		const arma::Col<double> &xi,
		arma::Col<double> &yi,
		const bool extrap){

		// check input
		assert(x.n_elem==y.n_elem);
		assert(!x.is_empty()); assert(!y.is_empty());
		assert(x.is_sorted("strictascend"));

		// allocate output
		yi.set_size(xi.n_elem);

		// interpolation
		{
			// find indexes in range
			const arma::Col<arma::uword> idx_mid = arma::find(xi>x.front() && xi<x.back());

			// check if any exist
			if(arma::any(idx_mid)){
				// get range for X
				const double xrange = x.back() - x.front();

				// find position relative to start and end
				const arma::Col<double> xrel = (xi(idx_mid) - x.front())/xrange;
				
				// convert this to an index
				arma::Col<arma::uword> xidx = arma::conv_to<arma::Col<arma::uword> >::from(xrel*(double)(x.n_elem-1));

				// // keep in bounds
				// xidx(arma::find(xidx==x.n_elem)) = xidx(arma::find(xidx==x.n_elem))-1;

				// get interpolation coordinates
				const arma::Col<double> x1 = x(xidx);
				const arma::Col<double> x2 = x(xidx+1);
				const arma::Col<double> y1 = y(xidx);
				const arma::Col<double> y2 = y(xidx+1);

				// linear interpolation
				yi(idx_mid) = (xi(idx_mid)-x1)%((y2-y1)/(x2-x1)) + y1;
			}
		}

		// extrapolation
		if(extrap){
			// check if extrapolation possible
			assert(x.n_elem>=2); assert(y.n_elem>=2);

			// find indexes beyond range
			const arma::Col<arma::uword> idx_beyond = arma::find(xi>=x.back());

			// check if any exist
			if(arma::any(idx_beyond)){
				// get last two points
				const double x1 = x(x.n_elem-2);
				const double x2 = x(x.n_elem-1);
				const double y1 = y(y.n_elem-2);
				const double y2 = y(y.n_elem-1);

				// calculate slope
				const double b = (y2-y1)/(x2-x1);

				// extrapolate
				yi(idx_beyond) = y2 + b*(xi(idx_beyond)-x2);
			}

			// find indexes before range
			const arma::Col<arma::uword> idx_before = arma::find(xi<=x.front());

			// check if any exist
			if(arma::any(idx_before)){
				// get last two points
				const double x1 = x(0);
				const double x2 = x(1);
				const double y1 = y(0);
				const double y2 = y(1);

				// calculate slope
				const double b = (y2-y1)/(x2-x1);

				// extrapolate
				yi(idx_before) = y1 - b*(x1-xi(idx_before));
			}
		}

		// extrapolation with fixed value
		else{
			yi(arma::find(xi<x.front() && xi>x.back())).fill(0.0);
		}
	}


	// find unique rows and sort
	arma::Mat<arma::uword> Extra::unique_rows(const arma::Mat<arma::uword> &M){
		// copy M
		arma::Mat<arma::uword> Mout = M;

		// sort rows
		for(arma::uword i=0;i<Mout.n_cols;i++){
			const arma::Col<arma::uword> sort_index = arma::stable_sort_index(Mout.col(i));
			Mout = Mout.rows(sort_index);
		} 

		// find unique 
		arma::Col<arma::uword> is_unique(Mout.n_rows);
		is_unique(0) = true;
		for(arma::uword i=1;i<Mout.n_rows;i++)
			is_unique(i) = arma::any(Mout.row(i)!=Mout.row(i-1));

		// select only unique rows
		Mout = Mout.rows(arma::find(is_unique));

		// return 
		return Mout;
	}

	// parallel implementation of sort
	arma::Row<arma::uword> Extra::parsort(const arma::Row<arma::uword> &vals){
		// for very short arrays
		if(vals.n_elem<60000)return arma::sort(vals);

		// get number of CPU's
		const arma::uword num_splits = 6;
		arma::uword num_subdivide = std::pow(2,num_splits);

		// split array
		arma::field<arma::Row<arma::uword> > split_vals(num_subdivide);

		// get number of elements
		const arma::uword num_elements = vals.n_elem;

		// get number of elements in each
		const arma::uword num_split = num_elements/num_subdivide+1;

		// fill arrays
		for(arma::uword i=0;i<num_subdivide;i++){
			split_vals(i) = vals.cols(i*num_split,std::min((i+1)*num_split-1,num_elements-1));
		}

		// sort split arrays in parallel using standard sorting
		parfor(0,num_subdivide,true,[&](int i, int) {
			split_vals(i) = arma::sort(split_vals(i));
		});

		// now perform merge
		for(arma::uword k=num_subdivide;k>1;k/=2){
			//for(arma::uword j=0;j<k/2;j++){
			parfor(0,k/2,true,[&](int j, int) {
				// which arrays
				const arma::uword a = j; 
				const arma::uword b = j+k/2;

				// number of merged values
				const arma::uword num_merged = split_vals(a).n_elem + split_vals(b).n_elem;

				// create new merged array
				arma::Row<arma::uword> merged_vals(num_merged);

				// walk over array and merge
				arma::uword idxa = 0, idxb = 0;
				for(arma::uword k=0;k<num_merged;k++){
					if(idxa>=split_vals(a).n_elem){
						merged_vals(k) = split_vals(b)(idxb); idxb++;
					}else if(idxb>=split_vals(b).n_elem){
						merged_vals(k) = split_vals(a)(idxa); idxa++;
					}else if(split_vals(a)(idxa)<split_vals(b)(idxb)){
						merged_vals(k) = split_vals(a)(idxa); idxa++;
					}else{
						merged_vals(k) = split_vals(b)(idxb); idxb++;
					}
				}

				// store merged vals
				split_vals(a) = merged_vals;
				split_vals(b).reset();
			});
		}

		// check output
		assert(split_vals(0).is_sorted());
		assert(split_vals(0).n_elem==num_elements);

		// return merged array
		return split_vals(0);
	}

	// make a new directory if it doesn't already exist
	// the directory will have default privileges
	void Extra::create_directory(const std::string &dirname){
		mkdir(dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	}

}}