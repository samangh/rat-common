/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "freecad.hh"

// code specific to Rat
namespace rat{namespace cmn{
	// constructor
	FreeCAD::FreeCAD(const boost::filesystem::path &fname, const std::string &model_name){
		// open file for writing
		fid_.open(fname.string());
		
		// ensure file is open
		assert(fid_.is_open());

		// set precision
		fid_ << std::fixed;
		fid_ << std::setprecision(7);

		// header write
		write_header(model_name);
	}

	// destructor
	FreeCAD::~FreeCAD(){
		// write footer
		write_footer();
		
		// close file
		if(fid_.is_open())fid_.close();
	}

	// factory
	ShFreeCADPr FreeCAD::create(const boost::filesystem::path &fname, const std::string &model_name){
		//return ShFreeCADPr(new FreeCAD);
		return std::make_shared<FreeCAD>(fname,model_name);
	}

	// set number of subdivisions
	void FreeCAD::set_num_sub(const arma::uword num_sub){
		num_sub_ = num_sub;
	}

	// write point list
	void FreeCAD::write_header(const std::string &model_name){
		// write header
		fid_<<"import FreeCAD,Draft,Part\n";
		fid_<<"doc = App.newDocument(\""<<model_name<<"\")\n";
	}

	// write edges
	void FreeCAD::write_edges( 
		const arma::field<arma::Mat<double> > &x,
		const arma::field<arma::Mat<double> > &y, 
		const arma::field<arma::Mat<double> > &z,
		const arma::Row<arma::uword> &section_idx,
		const arma::Row<arma::uword> &turn_idx,
		const std::string coil_name){

		// create group with same name as coil
		fid_<<"gr = doc.addObject(\"App::DocumentObjectGroup\",\""<<coil_name<<"\")\n";

		// walk over sections
		for(arma::uword i=0;i<x.n_elem;i++){
			// calculate subdivisions
			const arma::uword num_write = x(i).n_cols/num_sub_ + 1;

			// calculate number of subsections
			const arma::uword num_sub_int = x(i).n_cols/num_write + 1;

			// write to section
			for(arma::uword j=0;j<num_sub_int;j++){
				// walk over subsections
				const arma::uword idx1 = j*num_write;
				const arma::uword idx2 = std::min(x(i).n_cols-1,(j+1)*num_write);

				// write section
				write_section(x(i).cols(idx1,idx2),y(i).cols(idx1,idx2),
					z(i).cols(idx1,idx2),section_idx(i),turn_idx(i),j,coil_name);
			}
		}

	}

	// write section
	void FreeCAD::write_section(
		const arma::Mat<double> &x,
		const arma::Mat<double> &y, 
		const arma::Mat<double> &z,
		const arma::uword section_idx, 
		const arma::uword turn_idx,
		const arma::uword sub_idx,
		const std::string coil_name){

		// check for empty
		assert(!x.is_empty());
		assert(!y.is_empty());
		assert(!z.is_empty());

		// walk over edges
		for(arma::uword k=0;k<4;k++){
			// walk over coordinates
			for(arma::uword j=0;j<x.n_cols;j++){
				fid_<<"p"<<j<<" = FreeCAD.Vector(";
				fid_<<scale_*x(k,j)<<","<<scale_*y(k,j)<<","<<scale_*z(k,j);
				fid_<<")\n";
			}
			fid_<<"spline"<<k<<" = Draft.makeBSpline([";
			write_pointlist(x.n_cols);
			fid_<<"],closed=False)\n";
			fid_<<"spline"<<k<<".ViewObject.Visibility = False\n";
		}

		// ruled surfaces
		fid_<<"rule0 = doc.addObject('Part::RuledSurface','rule0')\n";
		fid_<<"rule0.Curve1 = (spline0,[''])\n";
		fid_<<"rule0.Curve2 = (spline1,[''])\n";
		fid_<<"rule0.ViewObject.Visibility = False\n";
		fid_<<"rule1 = doc.addObject('Part::RuledSurface','rule1')\n";
		fid_<<"rule1.Curve1 = (spline2,[''])\n";
		fid_<<"rule1.Curve2 = (spline3,[''])\n";
		fid_<<"rule1.ViewObject.Visibility = False\n";
		// %fprintf(fid_,'doc.recompute()\n');

		// % loft
		fid_<<"loft = doc.addObject('Part::Loft','";
		char str[77];
		std::sprintf(str,"_turn%02lli_sect%02lli_sub%02lli')",turn_idx,section_idx,sub_idx);
		fid_<<coil_name<<str<<"\n";
		fid_<<"loft.Sections = [rule0,rule1]\n";
		fid_<<"loft.Solid = True\n";
		fid_<<"loft.Ruled = False\n";

		// % set view colors etc
		fid_<<"loft.ViewObject.LineColor = (1.0,1.0,1.0,0.0)\n";
		fid_<<"loft.ViewObject.LineWidth = 0.5\n";
		fid_<<"loft.ViewObject.ShapeColor = (0.54,0.78,0.78,0.0)\n";
		fid_<<"loft.ViewObject.Deviation = 0.1\n";

		// % add object to group
		fid_<<"gr.addObject(loft)\n";

		// % compute geometry
		fid_<<"doc.recompute()\n";
	}

	// write point list
	void FreeCAD::write_pointlist(arma::uword num_points){
		
		// walk over points (except last)
		for(arma::uword j=0;j<num_points-1;j++){
			fid_<<"p"<<j<<",";
		}

		// last point
		fid_<<"p"<<num_points-1;
	} 

	// write footer to file
	void FreeCAD::write_footer(){
		fid_<<"doc.recompute()\n";
		fid_<<"Gui.SendMsgToActiveView(\"ViewFit\")";
	}

}}