/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>

// specific headers
#include "defines.hh"
#include "extra.hh"

// DESCRIPTION
// tests cross product and dot product

// main
int main(){
	// settings
	const arma::uword num_vec = 10000;
	
	// set random seed
	arma::arma_rng::set_seed(1001);

	// generate random vectors
	arma::Mat<double> R1(3,num_vec,arma::fill::randu);
	arma::Mat<double> R2(3,num_vec,arma::fill::randu);
	const arma::Row<double> dp = rat::cmn::Extra::dot(R1,R2)/(rat::cmn::Extra::vec_norm(R1)%rat::cmn::Extra::vec_norm(R2));

	// remove too similar vectors
	const arma::Row<arma::uword> idx = arma::find(dp<0.99).t();
	R1 = R1.cols(idx); R2 = R2.cols(idx);

	// calculate cross product
	const arma::Mat<double> R3 = rat::cmn::Extra::cross(R1,R2);

	// check vector orthogonality
	if(!arma::all(rat::cmn::Extra::dot(R1,R3)<1e-10))
		rat_throw_line("cross product inconsistent");
	if(!arma::all(rat::cmn::Extra::dot(R2,R3)<1e-10))
		rat_throw_line("cross product inconsistent");
	
	// return
	return 0;
}