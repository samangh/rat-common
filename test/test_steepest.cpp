/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// specific headers
#include "defines.hh"
#include "log.hh"
#include "steepest.hh"

// main
int main(){
	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// create solver object
	rat::cmn::ShSteepestPr myoptim = rat::cmn::Steepest::create();
	//mysolver->set_tolfun(1e-8);
	myoptim->set_gamma(0.2);
	myoptim->set_use_central_diff(true);
	myoptim->set_use_linesearch(true);

	// system function
	rat::cmn::StSysFun sysfun = [&](const arma::Col<double> &x){
		return arma::sum(0.1*x%x%x%x + 2*x%x + 2);
	};

	// jacobian function
	rat::cmn::StGradFun gradfun = [&](const arma::Col<double> &x){
		return (0.4*x%x%x + 4*x).eval();
	};

	// set objective function
	myoptim->set_systemfun(sysfun);
	//myoptim->set_finite_difference();
	myoptim->set_gradientfun(gradfun);

	// initial guess
	const arma::Col<double> x0 = {-2.65,1.25,-3.1,4.2};
	myoptim->set_initial(x0);

	// solve
	myoptim->optimise(lg);

	// check tolerance
	const arma::Col<double> x = myoptim->get_result();
	if(arma::any(arma::abs(x)>1e-4))
		rat_throw_line("tolerance not achieved");
}