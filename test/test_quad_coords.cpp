/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>

// specific headers
#include "defines.hh"
#include "elements.hh"
#include "gauss.hh"

// main
int main(){
	// settings
	const double D = 0.8; // box side length
	const double tol = 1e-5;
	const arma::uword Np = 100;
	const double dxtrap = 0.2;

	// define face corners
	const arma::Col<double>::fixed<3> R0 = {-D/2-dxtrap,-D/2,0}; 
	const arma::Col<double>::fixed<3> R1 = {+D/2+dxtrap,-D/2,0}; 
	const arma::Col<double>::fixed<3> R2 = {+D/2,+D/2,0}; 
	const arma::Col<double>::fixed<3> R3 = {-D/2,+D/2,0};

	// assemble matrix with nodes
	arma::Mat<double> Rn(3,4);
	Rn.col(0) = R0; Rn.col(1) = R1; Rn.col(2) = R2; Rn.col(3) = R3;

	// slope plane
	const double a = 0.1; const double b = 0.2;
	Rn.row(2) = a*Rn.row(0) + b*Rn.row(1);
	// Rn.swap_rows(2,1);

	// calculate face normal
	arma::Col<double>::fixed<3> N = arma::cross(Rn.col(1)-Rn.col(0), Rn.col(3)-Rn.col(0));
	N = N/arma::as_scalar(rat::cmn::Extra::vec_norm(N));

	// make random quadrilateral coordinates
	const arma::Mat<double> Rq1 = 4*(arma::Mat<double>(2,Np,arma::fill::randu)-0.5);

	// convert to carthesian coordinates
	arma::Mat<double> Rc = rat::cmn::Quadrilateral::quad2cart(Rn,Rq1);

	// offset points by facenormal
	// this to check if they actually go back
	// to the face 
	arma::Row<double> Noff(Np,arma::fill::randu); Noff -= 0.5;
	for(arma::uword i=0;i<3;i++)Rc.row(i) += Noff*N(i);

	// and back
	const arma::Mat<double> Rq2 = rat::cmn::Quadrilateral::cart2quad(Rn,Rc,tol);

	// check if the points remained the same
	// assert(arma::all(arma::all(arma::abs(Rq1-Rq2)/(std::sqrt(arma::as_scalar(arma::sum(
	//  	(Rn.col(0) - Rn.col(2))%(Rn.col(0) - Rn.col(2)),0))))<0.001)));
	if(!arma::all(arma::all(arma::abs(Rq1-Rq2)/(std::sqrt(arma::as_scalar(arma::sum(
	 	(Rn.col(0) - Rn.col(2))%(Rn.col(0) - Rn.col(2)),0))))<0.001))){
		rat_throw_line("quadrilateral coordinate transformation is not consistent");
	}

	// test mesh 
	// calculate gauss points
	// arma::Mat<double> v = Gauss::calc_gauss_points(5);
	// arma::Row<double> xg = v.row(0); 
	// arma::Row<double> wg = v.row(1);
	// std::cout<<arma::join_horiz(xg.t(),wg.t())<<std::endl;
	// arma::Col<double> Rctest = {0.4,0.4,0.1};
	// arma::Mat<double> Rqtest = rat::cmn::Quadrilateral::cart2quad(Rn,Rctest,tol);
	// Rqtest = arma::clamp(Rqtest,-1.0,1.0);
	// std::cout<<rat::cmn::Quadrilateral::quad2cart(Rn,Rqtest).t()<<std::endl;
	// arma::Mat<double> Rqgrd; arma::Row<double> wgrd;
	// rat::cmn::Quadrilateral::setup_source_grid(Rqgrd, wgrd, Rqtest,xg,wg);
	// arma::Mat<double> Rpoints = rat::cmn::Quadrilateral::quad2cart(Rn,Rqgrd);
	// std::cout<<arma::join_horiz(Rpoints.t(),wgrd.t())<<std::endl;
}
	
