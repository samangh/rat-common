/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>

// specific headers
#include "defines.hh"
#include "elements.hh"

// main
int main(){
	// settings
	const double D = 0.8; // box side length
	const double tol = 1e-5;
	const arma::uword N = 100;
	const double dxtrap = 0.1;

	// define box corners
	const arma::Col<double>::fixed<3> R0 = {-D/2-dxtrap,-D/2,-D/2}; 
	const arma::Col<double>::fixed<3> R1 = {+D/2+dxtrap,-D/2,-D/2}; 
	const arma::Col<double>::fixed<3> R2 = {+D/2,+D/2,-D/2}; 
	const arma::Col<double>::fixed<3> R3 = {-D/2,+D/2,-D/2};
	const arma::Col<double>::fixed<3> R4 = {-D/2-dxtrap,-D/2,+D/2}; 
	const arma::Col<double>::fixed<3> R5 = {+D/2+dxtrap,-D/2,+D/2}; 
	const arma::Col<double>::fixed<3> R6 = {+D/2,+D/2,+D/2}; 
	const arma::Col<double>::fixed<3> R7 = {-D/2,+D/2,+D/2}; 

	// assemble matrix with nodes
	arma::Mat<double> Rn(3,8);
	Rn.col(0) = R0; Rn.col(1) = R1; Rn.col(2) = R2; Rn.col(3) = R3;
	Rn.col(4) = R4; Rn.col(5) = R5; Rn.col(6) = R6; Rn.col(7) = R7;

	// make random quadrilateral coordinates
	const arma::Mat<double> Rq1 = 4*(arma::Mat<double>(3,N,arma::fill::randu)-0.5);

	// convert to carthesian coordinates
	const arma::Mat<double> Rc = rat::cmn::Hexahedron::quad2cart(Rn,Rq1);

	// and back
	const arma::Mat<double> Rq2 = rat::cmn::Hexahedron::cart2quad(Rn,Rc,tol);
	
	// check if the points remained the same
	if(!arma::all(arma::all(arma::abs(Rq1-Rq2)<tol)))rat_throw_line(
		"quadrilateral coordinate transformations for hexahedrons are inconsistent");

}
