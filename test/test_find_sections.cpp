/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>

// specific headers
#include "defines.hh"
#include "extra.hh"

// tests cross product and dot product

// main
int main(){
	// settings
	const arma::uword num_vec = 1000;
	const arma::uword max_val = 40;

	// set random seed
	arma::arma_rng::set_seed(1001);

	// make array with random integers
	arma::Row<arma::uword> myints = 
		arma::conv_to<arma::Row<arma::uword> >::from(
		arma::round(arma::Row<double>(
		num_vec,arma::fill::randu)*max_val));

	// sort array
	const arma::Row<arma::uword> mysortedints = arma::sort(myints);

	// run find sections
	const arma::Mat<arma::uword> sections = rat::cmn::Extra::find_sections(mysortedints);

	// get indexes
	const arma::Row<arma::uword> first_idx = sections.row(0);
	const arma::Row<arma::uword> last_idx = sections.row(1);

	// get first and last values
	const arma::Row<arma::uword> first_value = mysortedints.cols(first_idx);
	const arma::Row<arma::uword> last_value = mysortedints.cols(last_idx);

	// check that they are the same
	assert(arma::all(first_value==last_value));	
	assert(arma::all(first_idx.tail_cols(first_idx.n_cols-1)
		==last_idx.head_cols(last_idx.n_cols-1)+1));

	// walk over sections
	for(arma::uword i=0;i<sections.n_cols;i++){
		// check if all values are the same
		if(!arma::all(mysortedints.cols(first_idx(i),last_idx(i))==first_value(i)))
			rat_throw_line("find sections is inconsistent");
	}

	// return
	return 0;
}