/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// specific headers
#include "defines.hh"
#include "extra.hh"

// main
int main(){
	// settings
	arma::uword list_length = 1e7;
	arma::uword max_index = 1e5;

	// create a list of random numbers
	arma::Row<arma::uword> list = arma::conv_to<arma::Row<arma::uword> >::from(
		arma::Row<double>(list_length,arma::fill::randn)*max_index);

	// sort
	arma::Row<arma::uword> list_sorted = rat::cmn::Extra::parsort(list);

	// check sorted
	if(!list_sorted.is_sorted())rat_throw_line("sorting failed");


}