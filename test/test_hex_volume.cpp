/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>

// specific headers
#include "defines.hh"
#include "elements.hh"

// main
int main(){
	// settings
	double D = 0.5; // box side length

	// define box corners
	const arma::Col<double>::fixed<3> R0 = {-D/2,-D/2,-D/2}; 
	const arma::Col<double>::fixed<3> R1 = {+D/2,-D/2,-D/2}; 
	const arma::Col<double>::fixed<3> R2 = {+D/2,+D/2,-D/2}; 
	const arma::Col<double>::fixed<3> R3 = {-D/2,+D/2,-D/2};
	const arma::Col<double>::fixed<3> R4 = {-D/2,-D/2,+D/2}; 
	const arma::Col<double>::fixed<3> R5 = {+D/2,-D/2,+D/2}; 
	const arma::Col<double>::fixed<3> R6 = {+D/2,+D/2,+D/2}; 
	const arma::Col<double>::fixed<3> R7 = {-D/2,+D/2,+D/2}; 

	// assemble matrix
	arma::Mat<double> Rn(3,8);
	Rn.col(0) = R0; Rn.col(1) = R1; Rn.col(2) = R2; Rn.col(3) = R3;
	Rn.col(4) = R4; Rn.col(5) = R5; Rn.col(6) = R6; Rn.col(7) = R7;

	// get hexahedron to tetrahedron matrix
	const arma::Mat<arma::uword>::fixed<5,4> M = rat::cmn::Hexahedron::tetrahedron_conversion_matrix();

	// volume of tetrahedron from planes
	const arma::Row<double> V = rat::cmn::Tetrahedron::calc_volume(Rn,M.t());

	// volume should be 1/6 of the box volume
	if(!arma::all(arma::abs(V.cols(0,3)-std::pow(D,3)/6)<1e-12))rat_throw_line(
		"volume of corner tetrahedrons as part of a hexahedron is inconsistent with theory");
	if(!arma::all(arma::abs(V.col(4)-std::pow(D,3)/3)<1e-12))rat_throw_line(
		"volume of central tetrahedron as part of a hexahedron is inconsistent with theory");

	// return
	return 0;
}