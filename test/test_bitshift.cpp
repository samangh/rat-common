/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>

// specific headers
#include "defines.hh"
#include "extra.hh"

// DESCRIPTION
// tests the bitshift method added in Extra

// main
int main(){
	// generate test numbers
	const arma::uword N = 1000; const double max = 1000;
	const arma::Row<arma::uword> vals = 
		arma::conv_to<arma::Row<arma::uword> >::from(
		arma::floor(arma::Row<double>(1,N,arma::fill::randu)*max));
	
	// check normal workings
	if(arma::all((vals*2)!=rat::cmn::Extra::bitshift(vals,1)))
		rat_throw_line("rat::cmn::Extra::bitshift inconsistent for +1");
	if(arma::all((vals/2)!=rat::cmn::Extra::bitshift(vals,-1)))
		rat_throw_line("rat::cmn::Extra::bitshift inconsistent for -1");
	if(arma::all((vals*4)!=rat::cmn::Extra::bitshift(vals,2)))
		rat_throw_line("rat::cmn::Extra::bitshift inconsistent for +2");
	if(arma::all((vals/4)!=rat::cmn::Extra::bitshift(vals,-2)))
		rat_throw_line("rat::cmn::Extra::bitshift inconsistent for -2");
	if(arma::all((vals*8)!=rat::cmn::Extra::bitshift(vals,3)))
		rat_throw_line("rat::cmn::Extra::bitshift inconsistent for +3");
	if(arma::all((vals/8)!=rat::cmn::Extra::bitshift(vals,-3)))
		rat_throw_line("rat::cmn::Extra::bitshift inconsistent for -3");
	if(arma::all((vals*16)!=rat::cmn::Extra::bitshift(vals,4)))
		rat_throw_line("rat::cmn::Extra::bitshift inconsistent for +4");
	if(arma::all((vals/16)!=rat::cmn::Extra::bitshift(vals,-4)))
		rat_throw_line("rat::cmn::Extra::bitshift inconsistent for -4");
	if(arma::all((vals*32)!=rat::cmn::Extra::bitshift(vals,5)))
		rat_throw_line("rat::cmn::Extra::bitshift inconsistent for +5");
	if(arma::all((vals/32)!=rat::cmn::Extra::bitshift(vals,-5)))
		rat_throw_line("rat::cmn::Extra::bitshift inconsistent for -5");
	if(arma::all((vals*64)!=rat::cmn::Extra::bitshift(vals,6)))
		rat_throw_line("rat::cmn::Extra::bitshift inconsistent for +6");
	if(arma::all((vals/64)!=rat::cmn::Extra::bitshift(vals,-6)))
		rat_throw_line("rat::cmn::Extra::bitshift inconsistent for -6");

	// return
	return 0;
}