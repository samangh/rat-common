/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>
#include <functional>

// specific headers
#include "defines.hh"
#include "extra.hh"
#include "gmres.hh"
#include "log.hh"

// main
int main(){
	// settings
	const arma::uword N = 100;
	const arma::uword num_iter_max = 150;
	const arma::uword num_restart = 32;
	const double tol = 1e-6;

	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// system of equations
	arma::Mat<double> A(N,N,arma::fill::randu);
	A.diag(-1).fill(2.0);
	A.diag(0).fill(5.0);
	A.diag(+1).fill(2.0);
	const arma::Col<double> b(N,arma::fill::ones);

	// output vector (and initial guess)
	arma::Col<double> x(N,arma::fill::zeros);

	// setup preconditioner
	arma::Mat<double> M(N,N,arma::fill::zeros);
	for(int i=-2;i<=2;i++)M.diag(i) = A.diag(i);

	// decomposition
	arma::Mat<double> L, U, P;
	arma::lu(L, U, P, M);

	// generate solver
	rat::cmn::ShGMRESPr mygmres = rat::cmn::GMRES::create();

	// system function
	mygmres->set_systemfun(
		[A](const arma::Col<double> &x){
	 	return A*x;
	});

	// preconditioner
	mygmres->set_precfun(
		[P,L,U](const arma::Col<double> &r){	
		//return arma::solve(M,x);
		arma::Col<double> out = arma::solve(arma::trimatu(U),arma::solve(arma::trimatl(L),P*r,
			arma::solve_opts::fast),arma::solve_opts::fast);
		return out;
	});

	// solver settings
	mygmres->set_num_iter_max(num_iter_max);
	mygmres->set_num_restart(num_restart);
	mygmres->set_tol(tol);

	// solve system
	mygmres->solve(x,b,lg);

	// check result
	if(arma::any((A*x-b)/arma::norm(b)>tol))rat_throw_line("tolerance not achieved");

	// return
	return 0;
}