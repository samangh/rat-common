/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// specific headers
#include "defines.hh"
#include "newtonraphson.hh"

// main
int main(){
	// settings
	const arma::uword num_equations = 10;

	// create a log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// create solver object
	rat::cmn::ShNewtonRaphsonPr mysolver = rat::cmn::NewtonRaphson::create();
	mysolver->set_tolfun(1e-8);

	// system function
	rat::cmn::NRSysFun sysfun = [&](const arma::Col<double> &x){
		return (x%arma::flipud(x) + x - 1.0).eval();
	};

	// set objective function
	mysolver->set_systemfun(sysfun);
	mysolver->set_finite_difference();
	mysolver->set_use_central_diff(true);

	// initial guess
	const arma::Col<double> x0(num_equations,arma::fill::zeros);
	mysolver->set_initial(x0);

	// solve
	mysolver->solve(lg);

	// get x
	const arma::Col<double> x = mysolver->get_result();
	const arma::Col<double> res = arma::abs(sysfun(x));

	// check result
	if(arma::any(res>1e-5))rat_throw_line("tolerance not achieved");
}