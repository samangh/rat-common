/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>

// specific headers
#include "defines.hh"
#include "elements.hh"

// main
int main(){
	// settings
	double D = 0.8; // box side length
	arma::uword Npoints = 10000;

	// set random seed
	arma::arma_rng::set_seed(1001);

	// define box corners
	const arma::Col<double>::fixed<3> R0 = {-D/2,-D/2,-D/2}; 
	const arma::Col<double>::fixed<3> R1 = {+D/2,-D/2,-D/2}; 
	const arma::Col<double>::fixed<3> R2 = {+D/2,+D/2,-D/2}; 
	const arma::Col<double>::fixed<3> R3 = {-D/2,+D/2,-D/2};
	const arma::Col<double>::fixed<3> R4 = {-D/2,-D/2,+D/2}; 
	const arma::Col<double>::fixed<3> R5 = {+D/2,-D/2,+D/2}; 
	const arma::Col<double>::fixed<3> R6 = {+D/2,+D/2,+D/2}; 
	const arma::Col<double>::fixed<3> R7 = {-D/2,+D/2,+D/2}; 

	// assemble matrix with nodes
	arma::Mat<double> Rn(3,8);
	Rn.col(0) = R0; Rn.col(1) = R1; Rn.col(2) = R2; Rn.col(3) = R3;
	Rn.col(4) = R4; Rn.col(5) = R5; Rn.col(6) = R6; Rn.col(7) = R7;

	// setup mesh
	const arma::Col<arma::uword> nh = {0,1,2,3,4,5,6,7};

	// all elements inside
	const arma::Mat<double> Rs1 = D*(arma::Mat<double>(3,Npoints,arma::fill::randu)-0.5);
	const arma::Row<arma::uword> inside1 = rat::cmn::Hexahedron::is_inside(Rs1, Rn, nh);
	if(!arma::all(inside1==1))rat_throw_line(
		"point found outside hexahedron");

	// all elements outside
	const arma::Mat<double> Rs2 = D*(arma::Mat<double>(3,Npoints,arma::fill::randu)-0.5)+D;
	const arma::Row<arma::uword> inside2 = rat::cmn::Hexahedron::is_inside(Rs2, Rn, nh);
	if(!arma::all(inside1==1))rat_throw_line(
		"point found inside hexahedron");

	// some inside, some outside
	const arma::Mat<double> Rs3 = 1.5*D*(arma::Mat<double>(3,Npoints,arma::fill::randu)-0.5);
	const arma::Row<arma::uword> inside3 = rat::cmn::Hexahedron::is_inside(Rs3, Rn, nh);
	const arma::Mat<double> Rin = Rs3.cols(arma::find(inside2));
	if(!arma::all(arma::all(Rin<D/2)))rat_throw_line(
		"points inside hexahedron are too far from origin");

	// return
	return 0;
}