/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>

// specific headers
#include "defines.hh"
#include "extra.hh"

// DESCRIPTION
// tests linear interpolation function

// main
int main(){
	// settings
	const arma::uword num_interp = 100;
	const double xmin = 0.2;
	const double xmax = 8.4;
	const double ximin = -0.4;
	const double ximax = 14.2;
	const double num_vals = 12000;
	const double scale = 20;

	// create arrays
	const arma::Col<double> x = arma::linspace<arma::Col<double> >(xmin,xmax,num_interp);
	const arma::Col<double> y = arma::Col<double>(num_interp,arma::fill::randu)*scale-scale/2;
	const arma::Col<double> xi = arma::Col<double>(num_vals,arma::fill::randu)*(ximax-ximin)+ximin;

	// allocate output values 
	arma::Col<double> yi_reference, yi_lininterp;

	// perform interpolation
	rat::cmn::Extra::interp1(x,y,xi,yi_reference,"linear",true);
	rat::cmn::Extra::lininterp1f(x,y,xi,yi_lininterp,true);

	// check result
	if(arma::any((yi_lininterp-yi_reference)>1e-14))
		rat_throw_line("linear interpolation does not agree with reference");

	// return
	return 0;
}