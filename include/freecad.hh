/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef CMN_FREECAD_HH
#define CMN_FREECAD_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iostream>
#include <iomanip>
#include <cassert>
#include <boost/filesystem.hpp>

// specific headers
#include "extra.hh"

// code specific to Rat
namespace rat{namespace cmn{
	// shared pointer definition
	typedef std::shared_ptr<class FreeCAD> ShFreeCADPr;

	// template for coil
	class FreeCAD{
		// properties
		protected:
			// file out stream
			std::ofstream fid_; 

			// set scaling
			double scale_ = 1000;

			// subdivisions
			arma::uword num_sub_ = 1;

		// methods
		public:
			// constructor
			FreeCAD(const boost::filesystem::path &fname, const std::string &model_name);

			// destructor
			~FreeCAD();

			// factory
			static ShFreeCADPr create(const boost::filesystem::path &fname, const std::string &model_name);

			// set number of subdivisions
			void set_num_sub(const arma::uword num_sub);

			// write freecad python script file
			void write_header(const std::string &model_name);
			void write_edges(const arma::field<arma::Mat<double> > &x,const arma::field<arma::Mat<double> > &y, const arma::field<arma::Mat<double> > &z, const arma::Row<arma::uword> &section, const arma::Row<arma::uword> &turn, const std::string coil_name);
			void write_section(const arma::Mat<double> &x, const arma::Mat<double> &y, const arma::Mat<double> &z, const arma::uword section_idx, const arma::uword turn_idx, const arma::uword sub_idx, const std::string coil_name);
			void write_pointlist(arma::uword num_points);
			void write_footer();
	};

}}

#endif
