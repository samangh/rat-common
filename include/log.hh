/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef CMN_LOG_HH
#define CMN_LOG_HH

// general headers
#include <cassert>
#include <memory>
#include <stdio.h>
#include <stdarg.h>     /* va_list, va_start, va_arg, va_end */
#include <mutex>

// specific headers
#include "defines.hh"

// code specific to Rat
namespace rat{namespace cmn{
	// shared pointer definition for log
	typedef std::shared_ptr<class Log> ShLogPr;

	// shared pointer definition for no-output log
	typedef std::shared_ptr<class NullLog> ShNullLogPr;

	// output types
	//enum VerboseType {general,fmm};

	// logging to the terminal
	class Log{
		// properties
		protected:
			// number of indentations
			int num_indent_ = 0;

			// create a mutex
			std::mutex mtx_;

		// methods 
		public:
			// constructor
			Log(){

			}

			// factory
			static ShLogPr create(){
				return std::make_shared<Log>();
			}

			// virtual destructor (obligatory)
			virtual ~Log(){};

			// send text to logbook
			virtual void msg(const char *fmt, ...){
				// lock for thread safety
				mtx_.lock();

				// create indentation
				for(int i=0;i<num_indent_;i++)
					std::printf(" ");

				// process arguments and output
				va_list arg;
				va_start(arg, fmt);
				std::vprintf(fmt, arg);
				va_end(arg);

				// unlock
				mtx_.unlock();
			}

			// send text to logbook and change indentation afterwards
			virtual void msg(const int incr, const char *fmt, ...){
				// lock for thread safety
				mtx_.lock();

				// create indentation
				if(incr!=0)for(int i=0;i<num_indent_;i++)
					std::printf(" ");

				// process arguments and output
				va_list arg;
				va_start(arg, fmt);
				std::vprintf(fmt, arg);
				va_end(arg);

				// increment indentation
				assert((int)num_indent_>=-incr);
				if((int)num_indent_>=-incr)
					num_indent_+=incr;

				// unlock
				mtx_.unlock();
			}

			// only change indent
			virtual void msg(const int incr){
				// lock for thread safety
				mtx_.lock();

				// increment indentation
				assert((int)num_indent_>=-incr);
				if((int)num_indent_>=-incr)
					num_indent_+=incr;

				// unlock
				mtx_.unlock();
			}

			// new line
			virtual void newl(){
				// lock for thread safety
				mtx_.lock();

				// enter
				std::printf("\n");

				// unlock
				mtx_.unlock();
			}

			// access to indentation
			virtual int get_num_indent(){
				return num_indent_;
			}
	};

	// null logger (no output)
	// used as a placeholder when no log is present
	class NullLog: public Log{
		// methods
		public:
			// constructor
			NullLog(){};

			// factory
			static ShNullLogPr create(){
				return std::make_shared<NullLog>();
			}

			// destructor
			virtual ~NullLog(){};

			// send text to logbook
			virtual void msg(const char*, ...){}

			// send text to logbook and change indentation afterwards
			virtual void msg(const int, const char*, ...){}

			// only change indent
			virtual void msg(const int){}

			// new line
			virtual void newl(){}

			// access to indentation
			virtual int get_num_indent(){return 0;}
	};

}}

#endif