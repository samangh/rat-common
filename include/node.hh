/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CMN_NODE_HH
#define CMN_NODE_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <list>
#include <map>
#include <json/json.h>
#include <boost/filesystem.hpp>

#include "defines.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// shared pointer definition
	typedef std::shared_ptr<class Node> ShNodePr;
	typedef arma::field<ShNodePr> ShNodePrList;

	// node con
	typedef ShNodePr(*NodeFactory)();
	typedef std::map<std::string,NodeFactory> NodeFactoryMap;

	// list types
	typedef std::map<ShNodePr,arma::uword> SList;
	typedef std::map<arma::uword,ShNodePr> DSList;

	// circle distance function for distmesh
	class Node{
		public:
			// default constructor
			Node(){};
			virtual ~Node(){};

			// copy function
			virtual ShNodePr copy() const;
			template<typename T> std::shared_ptr<T> copy_covariant() const;

			// serialization
			virtual void serialize(Json::Value &js, SList &list) const;
			virtual void deserialize(const Json::Value &js, std::map<arma::uword,ShNodePr> &list, const NodeFactoryMap &factory_list, const boost::filesystem::path &pth);
			
			// serializing armadillo array 
			static Json::Value serialize_arma(const arma::Row<double> &V);
			static arma::Row<double> deserialize_arma(const Json::Value &js);

			// serialization of the nodes
			static Json::Value serialize_node(ShNodePr node, SList &list);
			static ShNodePr deserialize_node_core(const Json::Value &js, DSList &list, const NodeFactoryMap &factory_list, const boost::filesystem::path &pth);
			template<typename T> static std::shared_ptr<T> deserialize_node(const Json::Value &js, DSList &list, const NodeFactoryMap &factory_list, const boost::filesystem::path &pth);

			// json parse function
			static Json::Value parse_json(const boost::filesystem::path &fname);
	};

	// template function (must be in header)
	template<typename T> 
	std::shared_ptr<T> Node::deserialize_node(
		const Json::Value &js, DSList &list, 
		const NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){
		
		// get node
		ShNodePr node = Node::deserialize_node_core(js, list, factory_list, pth);
		
		// downcast to required type and check
		std::shared_ptr<T> castnode = std::dynamic_pointer_cast<T>(node);
		if(castnode==NULL && node!=NULL)rat_throw_line("downcast failed: " + js["type"].asString());

		// return downcast node
		return castnode;
	}

	// copy function
	template<typename T> std::shared_ptr<T> Node::copy_covariant() const{
		std::shared_ptr<T> typed_copy = std::dynamic_pointer_cast<T>(copy());
		if(typed_copy==NULL)rat_throw_line("could not cast copy to specified type");
		return typed_copy;
	}

}}

#endif
