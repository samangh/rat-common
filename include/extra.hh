/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef CMN_EXTRA_HH
#define CMN_EXTRA_HH

// general headers
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream

#include <armadillo> 
#include <complex>
#include <cmath>
#include <cassert>

#include <sys/stat.h> // for mkdir

// specific headers
#include "parfor.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// library of math functions, which are not specifically class related,
	// that are used throughout the code
	class Extra{
		public:
			// displaying of matrices
			static void display_mat(const arma::Mat<std::complex<double> > &M);
			static void display_mat(const arma::SpMat<std::complex<double> > &M);
			static void display_mat(const arma::Mat<double> &M);
			static void display_mat(const arma::SpMat<double> &M);	
			
			// vector operations for (3xN) matrices
			static arma::Mat<double> cross(const arma::Mat<double> &R1, const arma::Mat<double> &R2);
			static arma::Row<double> dot(const arma::Mat<double> &R1, const arma::Mat<double> &R2);
			static arma::Row<double> vec_norm(const arma::Mat<double> &R1);

			// rotation matrix setup
			static arma::Mat<double>::fixed<3,3> create_rotation_matrix(const double phi, const double theta, const double psi);
			static arma::Mat<double>::fixed<3,3> create_rotation_matrix(const arma::Col<double>::fixed<3> &V, const double alpha);
			static arma::Mat<double>::fixed<3,3> create_rotation_matrix(const double ux, const double uy, const double uz, const double alpha);

			// functions for calculating random coordinates in (3xN) format
			static arma::Mat<double> random_coordinates(const double xc, const double yc, const double zc, const double size, const arma::uword N);
			static arma::Mat<double> random_coordinates(arma::Col<double>::fixed<3> Rc, const double size, const arma::uword N);

			// some basic math functions
			static arma::Mat<arma::uword> bitshift(const arma::Mat<arma::uword> &M, const int nshift);
			static arma::Mat<arma::uword> modulus(const arma::Mat<arma::uword> &A, const arma::uword i);
			static double round(const double d);
			static double sign(const double s);

			// function for subdividing arrays
			static arma::Mat<arma::uword> find_sections(const arma::Row<arma::uword> &M);
			static arma::Mat<arma::uword> set_sections(const arma::Mat<arma::uword> &indices);
			static arma::Row<arma::uword> expand_indices(const arma::Mat<arma::uword> &indices, const arma::uword num_dim);

			// cell2mat implementation
			template<typename T> static T field2mat(const arma::field<T> &fld);

			// field array reversal
			static void reverse_field(arma::field<arma::Mat<double> > &A);

			// interpolation function for scalar input XI
			static double interp1(const arma::Mat<double> &X, const arma::Mat<double> &Y, const double XI, const char *type = "linear", const bool extrap = false);

			// interpolion function with extrapolation
			static void interp1(const arma::Col<double> &x, const arma::Col<double> &y, const double &xi, double &yi, const char *type = "linear", const bool extrap = false);
			static void interp1(const arma::Col<double> &x, const arma::Col<double> &y, const arma::Col<double> &xi, arma::Col<double> &yi, const char *type = "linear", const bool extrap = false);

			// fast interpolation function
			static void lininterp1f(const arma::Col<double> &x, const arma::Col<double> &y, const arma::Col<double> &xi, arma::Col<double> &yi, const bool extrap = true);

			// unique rows
			static arma::Mat<arma::uword> unique_rows(const arma::Mat<arma::uword> &M);

			// parallel sorting
			static arma::Row<arma::uword> parsort(const arma::Row<arma::uword> &vals);

			// making directory
			static void create_directory(const std::string &dirname);
	};

}}

#endif