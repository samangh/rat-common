/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// adapted from: https://ch.mathworks.com/matlabcentral/fileexchange/43097-newton-raphson-solver

// include guard
#ifndef CMN_STEEPEST_HH
#define CMN_STEEPEST_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iomanip>
#include <functional>
#include <cassert>

// specific headers
#include "defines.hh"
#include "parfor.hh"
#include "log.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// shared pointer definition
	typedef std::shared_ptr<class Steepest> ShSteepestPr;

	// gradient function
	typedef std::function<arma::Col<double>(const arma::Col<double>&) > StGradFun;

	// system function
	typedef std::function<double(const arma::Col<double>&) > StSysFun;

	// newton raphson non-linear solver
	class Steepest{
		// properties
		private:
			// settings
			double delta_ = 1e-6;
			double gamma_ = 0.5;
			double tolx_ = 1e-4;
			arma::uword num_iter_max_ = 1000;
			bool use_parallel_ = false;
			bool use_linesearch_ = true;
			bool use_central_diff_ = false;

			// functions describing problem
			StSysFun systemfun_;
			StGradFun gradientfun_;
			
			// solver 
			arma::Col<double> x0_;
			arma::Col<double> x_;
			double fval_;

		// methods
		public:
			// constructor
			Steepest();

			// factory
			static ShSteepestPr create();

			// set system function to optimise
			void set_systemfun(StSysFun systemfun);

			// set/or assemble gradient function
			void set_finite_difference();

			// set/or assemble gradient function
			void set_gradientfun(StGradFun gradientfun);

			// set initial guess for x
			void set_initial(const arma::Col<double> &x0);

			// set display flag
			void set_use_parallel(const bool use_parallel);
			void set_use_central_diff(const bool central_diff);
			void set_use_linesearch(const bool use_linesearch);
			void set_delta(const double delta);
			void set_gamma(const double gamma);
			void set_tolx(const double tolx);

			// run optimisation
			void optimise(ShLogPr lg = NullLog::create());

			// get result vector
			arma::Col<double> get_result() const;

			// gradient approximation
			static arma::Col<double> approximate_gradient(const arma::Col<double> &x, const double dx, const bool use_parallel, const bool central_diff, StSysFun sysfn);

			// display function
			// static void default_displayfun(const arma::uword n, const double fval, const double stepnorm);
	};

}}

#endif
