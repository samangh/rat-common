/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef CMN_PARFOR_HH
#define CMN_PARFOR_HH

// general headers
#include <thread> 
#include <atomic>
#include <future>
#include <vector>

// parallel for loop implementation as template class. 
// Code modified from Andy Thomason:
// http://www.andythomason.com/2016/08/21/c-multithreading-an-effective-parallel-for-loop/

// a parfor loop can be made like this
// parfor(10,20,true,[&](int idx, int cpu) {
//        printf("task %d running on cpu %d\n", idx, cpu);
//     }
// );

// get number of available cpus for
// parfor loops
// int parfor_num_cpus(){
// 	return std::thread::hardware_concurrency();
// }

// code specific to Rat
namespace rat{namespace cmn{
	// parallel-for template class
	template <class F>
	void parfor(arma::uword begin, arma::uword end, bool enable_parallel, F fn) {
		// make index
		std::atomic<arma::uword> idx;
		idx = begin;

		// fire asynchronous threads
		// don't run as threads when loop only consists of one walk
		if(enable_parallel==true && end-begin>0){
			// get number of cpu's
			int num_cpus = std::thread::hardware_concurrency();
			char* str = std::getenv("RAT_NUM_THREADS");
			if(str!=NULL)num_cpus = std::stod(std::string(str));
			
			// get number of available CPU cores
			// int num_cpus = std::thread::hardware_concurrency();
			std::vector<std::future<void> > futures(num_cpus);

			// walk over available cpus
			for (int cpu = 0; cpu != num_cpus; cpu++) {
				futures[cpu] = std::async(std::launch::async,
					[cpu, &idx, end, &fn]() {
						// keep firing tasks untill run out of idx
						for (;;) {
							// atomic increment of index so that no two threads can do same task
							arma::uword i = idx++; 

							// check if last index
							if (i >= end) break;

							// run loop content with i
							fn(i, cpu);
						}
					}
				);
			}

			// make sure all threads are finished
			for (int cpu = 0; cpu != num_cpus; cpu++) {
				futures[cpu].get();
			}
		}

		// just run as regular loop
		else{
			for(arma::uword i = begin;i<end;i++){
				fn(i,0);
			}
		}
	}

}}

#endif