/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef CMN_DEFINES_HH
#define CMN_DEFINES_HH

// general headers
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

// separator
const char filesep =
#ifdef _WIN32
	'\\';
#else
	'/';
#endif

// Terminal color definitions
// color definitions
#define KNRM "\x1B[0m"
#define KBLD "\033[1m"
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KBLU "\x1B[34m"
#define KMAG "\x1B[35m"
#define KCYN "\x1B[36m"
#define KWHT "\x1B[37m"

// custom runtime_error implementation
// in order to include filename and line number
class rat_error: public std::runtime_error{
	private:
		std::string msg;
	public:
		// constructor
		rat_error(const std::string &arg, const char *file, const char *function, int line) :
		std::runtime_error(arg) {
			std::ostringstream o;
			o << KRED << KBLD << "error: " << KNRM << file << ":" << function << ":" << 
				KGRN << KBLD << line << KNRM << ": " << std::endl << " " KYEL << arg << KNRM;
			msg = o.str();
		}
		
		// destructor
		~rat_error() throw() {}
		const char *what() const throw() {
			return msg.c_str();
		}
};

// macro for error
#define rat_throw_line(arg) throw rat_error(arg, __FILE__, __FUNCTION__, __LINE__);


#endif