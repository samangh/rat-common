/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CMN_SERIALIZER_HH
#define CMN_SERIALIZER_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <functional>
#include <json/json.h>
#include <cstdlib>
#include <set>
#include <boost/filesystem.hpp>

#include "node.hh"
#include "log.hh"

// code specific to Rat
namespace rat{namespace cmn{

	// shared pointer definition
	typedef std::shared_ptr<class Serializer> ShSerializerPr;

	// serialization code for distmesh models
	class Serializer{
		// registration
		protected:
			// version number
			std::string json_version_ = "1.0.0";

			// list of object constructors
			NodeFactoryMap factory_list_;

			// json root
			Json::Value root_json_;

			// path to root
			boost::filesystem::path pth_;

		// methods
		public:
			// constructor
			Serializer();
			Serializer(ShNodePr root_node);
			Serializer(const boost::filesystem::path &fname);

			// destructor
			virtual ~Serializer(){};

			// factory
			static ShSerializerPr create();
			static ShSerializerPr create(ShNodePr root_node);
			static ShSerializerPr create(const boost::filesystem::path &fname);

			// import/export to text file
			void import_json(const boost::filesystem::path &fname);
			void export_json(const boost::filesystem::path &fname);

			// tree construction
			ShNodePr construct_tree_core();
			void flatten_tree(ShNodePr root_node);
			template<typename T> std::shared_ptr<T> construct_tree();

			// registration
			void register_factory(const std::string &str,NodeFactory factory);
			template<typename T> void register_factory();
			void list_factories(ShLogPr lg) const;

			// get factory list
			NodeFactoryMap get_factory_list() const;

			// method for registering constructors
			// to be overridden
			virtual void register_constructors(){};

			// creation
			template<typename T> std::shared_ptr<T> json2tree(const std::string &fname);
	};

	// template functions (must be in header file)
	template<typename T> std::shared_ptr<T> Serializer::construct_tree(){
		std::shared_ptr<T> tree = std::dynamic_pointer_cast<T>(construct_tree_core());
		if(tree==NULL)rat_throw_line("could not cast tree to requested type");
		return tree;
	}

	// template functions (must be in header file)
	template<typename T> std::shared_ptr<T> Serializer::json2tree(const std::string &fname){
		if(factory_list_.size()==0)register_constructors(); 
		import_json(fname); return construct_tree<T>();
	}

	// alternative registration using template
	template<typename T> void Serializer::register_factory(){
		register_factory(T::get_type(), []()->rat::cmn::ShNodePr{return std::make_shared<T>();} );
	}

}}

#endif
