/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// this code was translated to C++ an adapted from:
// https://ch.mathworks.com/matlabcentral/fileexchange/43097-newton-raphson-solver

// include guard
#ifndef CMN_NEWTON_RAPHSON_HH
#define CMN_NEWTON_RAPHSON_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iomanip>
#include <functional>
#include <cassert>

// specific headers
#include "defines.hh"
#include "parfor.hh"
#include "log.hh"

// code specific to Rat
namespace rat{namespace cmn{
	// shared pointer definition
	typedef std::shared_ptr<class NewtonRaphson> ShNewtonRaphsonPr;

	// jacobian function
	typedef std::function<arma::Mat<double>(const arma::Col<double>&) > NRJacFun;

	// system function
	typedef std::function<arma::Col<double>(const arma::Col<double>&) > NRSysFun;

	// newton raphson non-linear solver
	class NewtonRaphson{
		// properties
		private:
			// settings
			bool use_parallel_ = false;
			bool use_central_diff_ = false;
			double tolx_ = 1e-12; // x tolerance
			double tolfun_ = 1e-6; // function tolerance
			arma::uword num_iter_max_ = 150; // maximum number of iterations
			double alpha_ = 1e-4; // criteria for decrease
			double min_lambda_ = 0.1; // minimum lambda
			double max_lambda_ = 0.5; // maximum lambda
			double delta_ = 1e-6; // finite difference stepsize

			// solver functions
			NRSysFun systemfun_;
			NRJacFun jacfun_;
			
			// initial guess vector
			arma::Col<double> x0_; 

			// solution vector
			arma::Col<double> x_; 

			// internal jacobian matrix
			arma::Mat<double> J_;

			// output statistics
			arma::sword flag_ = 0;
			arma::uword num_iter_;
			double resnorm_; // residual norm
			double stepnorm_;
			double lambda_; // lambda
			double rc_; // reciprocal condition
			arma::Col<double> F_; // function value
			arma::Col<double> dx_; // stepsize

		// methods
		public:
			// constructor
			NewtonRaphson();

			// factory
			static ShNewtonRaphsonPr create();

			// setting of system function and start point
			void set_systemfun(NRSysFun fn);
			void set_jacfun(NRJacFun fn);

			// jacobian function with finite difference
			void set_finite_difference();
		
			// set initial guess for x
			void set_initial(const arma::Col<double> &x0);

			// settings
			void set_use_parallel(const bool use_parallel);
			void set_use_central_diff(const bool central_diff);
			
			void set_display(const bool display);
			void set_tolx(const double tolx);
			void set_tolfun(const double tolfun);
			void set_delta(const double delta);
			void set_num_iter_max(const arma::uword num_iter_max);
			
			// solver functions
			void solve(ShLogPr lg = NullLog::create());

			// getting
			arma::Col<double> get_result() const;

			// finite difference approximation of the jacobian matrix
			// this will be used if no viable jacobian function assigned
			static arma::Mat<double> approximate_jacobian(const arma::Col<double> &x, const double dx, const bool use_parallel, const bool central_diff, NRSysFun sysfn);
	};

}}

#endif
