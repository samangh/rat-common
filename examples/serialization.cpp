/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general headers
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// specific headers
#include "node.hh"
#include "serializer.hh"

// shared pointer definition
typedef std::shared_ptr<class Animal> ShAnimalPr;
typedef std::shared_ptr<class Bear> ShBearPr;
typedef std::shared_ptr<class Duck> ShDuckPr;
typedef std::shared_ptr<class Farmer> ShFarmerPr;


class Animal: public rat::cmn::Node{
	public:
		virtual ~Animal(){};
};

class Bear: public Animal{
	public:
		Bear(){};
		rat::cmn::ShNodePr copy() const override{return Bear::create();};
		static std::string get_type(){return "cmn::bear";};
		void serialize(Json::Value &js, rat::cmn::SList &) const override{
			js["type"] = get_type();
			js["says"] = "growl";
		}
		static ShBearPr create(){return std::make_shared<Bear>();};
};

class Duck: public Animal{
	public:
		Duck(){};
		rat::cmn::ShNodePr copy() const override{return Duck::create();};
		static std::string get_type(){return "cmn::duck";};
		void serialize(Json::Value &js, rat::cmn::SList &) const override{
			js["type"] = get_type();
			js["says"] = "quack";
		}
		static ShDuckPr create(){return std::make_shared<Duck>();};
};

class Farmer: public Animal{
	private:
		std::list<ShAnimalPr> animals_;

	public:
		Farmer(){};
		rat::cmn::ShNodePr copy() const override{
			ShFarmerPr farmer_copy = Farmer::create();
			for(auto it = animals_.begin(); it!=animals_.end(); it++)
				farmer_copy->animals_.push_back((*it)->copy_covariant<Animal>());
			return farmer_copy;
		};
		static std::string get_type(){return "cmn::farmer";}
		void give(ShAnimalPr animal){animals_.push_back(animal);}
		void serialize(Json::Value &js, rat::cmn::SList &list) const override{
			js["type"] = get_type();
			js["says"] = "hi";
			for(std::list<ShAnimalPr>::const_iterator it = animals_.begin();it!=animals_.end();it++){
				js["animals"].append(Node::serialize_node((*it),list));
			}
		}
		void deserialize(const Json::Value &js, rat::cmn::DSList &list, const rat::cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override{
			arma::uword num_animals = js["animals"].size();
			for(arma::uword i=0;i<num_animals;i++){
				animals_.push_back(Node::deserialize_node<Animal>(js["animals"].get(i,0), list, factory_list, pth));
			}
		}
		static ShFarmerPr create(){return std::make_shared<Farmer>();}
};

// main
int main(){
	// create a duck
	ShDuckPr duck1 = Duck::create();
	ShDuckPr duck2 = Duck::create();
	ShFarmerPr farmer1 = Farmer::create();
	ShFarmerPr farmer2 = Farmer::create();
	ShFarmerPr farmer3 = Farmer::create();
	farmer1->give(farmer2);
	farmer1->give(farmer3);
	farmer2->give(duck1);
	farmer3->give(duck1);
	farmer1->give(duck2);
	farmer3->give(duck2);

	ShFarmerPr farmer4 = farmer1->copy_covariant<Farmer>();

	// write to output file
	rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();
	slzr->flatten_tree(farmer4); slzr->export_json("test.json");

	slzr->register_factory(Bear::get_type(), []()->rat::cmn::ShNodePr{return Bear::create();} );
	slzr->register_factory(Farmer::get_type(), []()->rat::cmn::ShNodePr{return Farmer::create();} );
	slzr->register_factory(Duck::get_type(), []()->rat::cmn::ShNodePr{return Duck::create();} );

	// go back 
	slzr->import_json("./../release/../release/test.json");
	ShFarmerPr tree = slzr->construct_tree<Farmer>();

	// write again
	slzr->flatten_tree(tree); slzr->export_json("test2.json");
}